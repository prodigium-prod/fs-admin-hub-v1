const express = require("express");
const { catchErrors } = require("../handlers/errorHandlers");

const router = express.Router();

const adminController = require("../controllers/adminController");
const clientController = require("../controllers/clientController");

const leadController = require("../controllers/leadController");
const productController = require("../controllers/productController");
const orderController = require("../controllers/orderController");
const cleanerController = require("../controllers/cleanerController");
const newsController = require("../controllers/newsController");
const membershipController = require("../controllers/membershipController");
const voucherController = require("../controllers/voucherController");
const userController = require("../controllers/userController");
const usersAdditionalController = require("../controllers/usersAdditionalController");
const orderBookingController = require("../controllers/orderBookingController");
const categoryController = require("../controllers/categoryController");
const trackingCleanerController = require("../controllers/trackingCleanerController");
const AdditionalService = require("../controllers/additionalServiceController");
const serviceController = require("../controllers/serviceController");

// //_______________________________ Admin management_______________________________
//
// router.route("/admin/create").post(catchErrors(adminController.create));
// router.route("/admin/read/:id").get(catchErrors(adminController.read));
// router.route("/admin/update/:id").patch(catchErrors(adminController.update));
// router.route("/admin/delete/:id").delete(catchErrors(adminController.delete));
// router.route("/admin/search").get(catchErrors(adminController.search));
// router.route("/admin/list").get(catchErrors(adminController.list));
//
// router
//   .route("/admin/password-update/:id")
//   .patch(catchErrors(adminController.updatePassword));
//list of admins ends here

//_____________________________________ API for clients __________________________
router.route("/client/create").post(catchErrors(clientController.create));
router.route("/client/read/:id").get(catchErrors(clientController.read));
router.route("/client/update/:id").patch(catchErrors(clientController.update));
router.route("/client/delete/:id").delete(catchErrors(clientController.delete));
router.route("/client/search").get(catchErrors(clientController.search));
router.route("/client/list").get(catchErrors(clientController.list));

//_____________________________________ API for leads ___________________________
router.route("/lead/create").post(catchErrors(leadController.create));
router.route("/lead/read/:id").get(catchErrors(leadController.read));
router.route("/lead/update/:id").patch(catchErrors(leadController.update));
router.route("/lead/delete/:id").delete(catchErrors(leadController.delete));
router.route("/lead/search").get(catchErrors(leadController.search));
router.route("/lead/list").get(catchErrors(leadController.list));

//_____________________________________ API for products ___________________________
router.route("/product/create").post(catchErrors(productController.create));
router.route("/product/read/:id").get(catchErrors(productController.read));
router.route("/product/update/:id").patch(catchErrors(productController.update));
router.route("/product/delete/:id").delete(catchErrors(productController.delete));
router.route("/product/search").get(catchErrors(productController.search));
router.route("/product/list").get(catchErrors(productController.list));

//_____________________________________ API for products ___________________________
router.route("/category/create").post(catchErrors(categoryController.create));
router.route("/category/read/:id").get(catchErrors(categoryController.read));
router.route("/category/update/:id").patch(catchErrors(categoryController.update));
router.route("/category/delete/:id").delete(catchErrors(categoryController.delete));
router.route("/category/search").get(catchErrors(categoryController.search));
router.route("/category/list").get(catchErrors(categoryController.list));

router.route("/cleaner/list").get(catchErrors(cleanerController.list));
router.route("/users/list").get(catchErrors(userController.list));
router.route("/users/update/:id").patch(catchErrors(userController.update));
router.route("/order/update/:id").patch(catchErrors(orderController.update));
router.route("/order/list").get(catchErrors(orderController.list));


router.route("/users-additional/list").get(catchErrors(usersAdditionalController.listUserAdditional));
router.route("/users-additional/read/:id").get(catchErrors(usersAdditionalController.read));
router.route("/users-additional/create").post(catchErrors(usersAdditionalController.createUserAdditional));
router.route("/users-additional/update/:id").patch(catchErrors(usersAdditionalController.update));
router.route("/users-additional/delete/:id").delete(catchErrors(usersAdditionalController.delete));
router.route("/users-additional/search").get(catchErrors(usersAdditionalController.search));

//_____________________________________ API for News ___________________________
router.route("/news/create").post(catchErrors(newsController.create));
router.route("/news/read/:id").get(catchErrors(newsController.read));
router.route("/news/update/:id").patch(catchErrors(newsController.update));
router.route("/news/delete/:id").delete(catchErrors(newsController.delete));
router.route("/news/search").get(catchErrors(newsController.search));
router.route("/news/list").get(catchErrors(newsController.list));

//_____________________________________ Membership for News ___________________________
router.route("/membership/create").post(catchErrors(membershipController.create));
router.route("/membership/read/:id").get(catchErrors(membershipController.read));
router.route("/membership/update/:id").patch(catchErrors(membershipController.update));
router.route("/membership/delete/:id").delete(catchErrors(membershipController.delete));
router.route("/membership/search").get(catchErrors(membershipController.search));
router.route("/membership/list").get(catchErrors(membershipController.list));

//_____________________________________ Voucher for News ___________________________
router.route("/voucher/create").post(catchErrors(voucherController.create));
router.route("/voucher/read/:id").get(catchErrors(voucherController.read));
router.route("/voucher/update/:id").patch(catchErrors(voucherController.update));
router.route("/voucher/delete/:id").delete(catchErrors(voucherController.delete));
router.route("/voucher/search").get(catchErrors(voucherController.search));
router.route("/voucher/list").get(catchErrors(voucherController.list));

//_____________________________________ AdditionalServices  ___________________________
router.route("/additional-service/create").post(catchErrors(AdditionalService.create));
router.route("/additional-service/read/:id").get(catchErrors(AdditionalService.read));
router.route("/additional-service/update/:id").patch(catchErrors(AdditionalService.update));
router.route("/additional-service/delete/:id").delete(catchErrors(AdditionalService.delete));
router.route("/additional-service/search").get(catchErrors(AdditionalService.search));
router.route("/additional-service/list").get(catchErrors(AdditionalService.list));

//_____________________________________ Order Urgent for Order ___________________________
// router.route("/order/create").post(catchErrors(orderUrgentController.create));
// router.route("/order-booking/create").post(catchErrors(orderBookingController.create));
router.route("/order-booking/list").get(catchErrors(orderBookingController.listOrderWeb));
router.route("/order-booking/count").get(catchErrors(orderBookingController.countBalanceOrder));
router.route("/order-booking/validate").post(catchErrors(orderBookingController.validateOrder));
router.route("/order-booking/status").post(catchErrors(orderBookingController.updateStatusOrder));


router.route("/tracking-cleaner/list").get(catchErrors(trackingCleanerController.list));
router.route("/service/list/:option").get(catchErrors(serviceController.listServiceOption));
router.route("/add-on/list").get(catchErrors(AdditionalService.listAdditionalService));
// router.route("/tracking-cleaner/export").post(catchErrors(trackingCleanerController.export));

module.exports = router;