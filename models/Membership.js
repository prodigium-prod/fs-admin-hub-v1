const mongoose = require('mongoose');

const membershipSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    point: {
        type: Number,
        required: true
    },
    discount: {
        type: Number,
        required: true
    },
});

module.exports = mongoose.model('Membership', membershipSchema);
