const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const newsSchema = new Schema({
    title: {type: String, default: null},
    desc: {type: String, default: null},
    image: {type: String, default: null},
    status: {type: String, default: null},
    lang: {type: String, default: null},
}, {timestamps: true});

module.exports = mongoose.model("News", newsSchema);