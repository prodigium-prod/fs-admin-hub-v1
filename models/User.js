const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const userSchema = new Schema({
    name: {type: String, default: null},
    password: {type: String, default: null},
    phone: {type: String, default: null},
    role: {type: String, default: null},
    email: {type: String, default: null},
    date_birth: {type: String, default: null},
    photo_user: {type: String, default: null},
    address: {type: String, default: null},
    member: {type: String, default: null},
    referral_code: {type: String, default: null},
    point: {type: Number, default: 0},
    auth: {type: Number, default: 0},
    vouchers: [{
        type: mongoose.Schema.Types.Mixed, default: null
    }],
    postal_code: {type: Number, default: null},
    isLoggedIn: {type: String, default: null},
    cleaner_detail: {type: mongoose.Schema.Types.ObjectId, ref: 'CleanerDetail', default : null},
}, {timestamps: true});

module.exports = mongoose.model("User", userSchema);