const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const categorySchema = new Schema({
    name: {type: String, required: true},
}, {timestamps: true});

module.exports = mongoose.model("Category", categorySchema);
