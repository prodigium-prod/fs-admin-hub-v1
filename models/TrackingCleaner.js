const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const trackingCleanerSchema = new mongoose.Schema({
    name: {type: String, required: true},
    cleaner_id: {type: String, required: true},
    status: {type: String, required: true},
    lat_long: {type: String, default: null},
    address: {type: String, default: null},
    datetime: {type: String, required: true},
}, {timestamps: true});

module.exports = mongoose.model('TrackingCleaner', trackingCleanerSchema);