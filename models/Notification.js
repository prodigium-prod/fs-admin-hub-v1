const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const notificationSchema = new Schema({
    order_id: {type: String,default: null},
    fraud_status: {type: String, default: null},
    transaction_status: {type: String,default: null}
}, {timestamps: true})

module.exports = mongoose.model("Notification", notificationSchema);