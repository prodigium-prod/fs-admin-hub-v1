const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const serviceSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    option: { type: String, required: true },
    price: { type: String, default: null}
});

module.exports = mongoose.model("Service", serviceSchema);