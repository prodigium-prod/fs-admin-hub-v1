const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const AdditionalServicesSchema = new Schema({
    category: { type: String, required: true },
    type: { type: String, required: true },
    type_option: { type: String , default: null},
    type_class: { type: String , default: null},
    unit: { type: String },
    price: { type: Number},
    total: { type: Number, default: 1}
});

module.exports = mongoose.model("AdditionalServices", AdditionalServicesSchema);