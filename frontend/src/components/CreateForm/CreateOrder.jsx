import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import moment from "moment";
import {useCrudContext} from "@/context/crud";
import {selectCreatedItem} from "@/redux/crud/selectors";
import {Button, Form, Input, Spin, Modal, message} from "antd";
import Loading from "@/components/Loading";
import PlacesAutocomplete, {geocodeByAddress, getLatLng} from "react-places-autocomplete";
import MapPicker from "react-google-map-picker";
import {LoadingOutlined} from "@ant-design/icons";
import axios from 'axios';

export default function CreateOrder({config, formElements}) {
    let {entity} = config;
    const dispatch = useDispatch();
    const {isLoading, isSuccess} = useSelector(selectCreatedItem);
    const {crudContextAction} = useCrudContext();
    const {panel, collapsedBox, readBox} = crudContextAction;
    const [form] = Form.useForm();

    const DefaultLocation = {lat: -6.2087634, lng: 106.845599};
    const DefaultZoom = 10;
    const [defaultLocation, setDefaultLocation] = useState(DefaultLocation);
    const [location, setLocation] = useState(defaultLocation);
    const [zoom, setZoom] = useState(DefaultZoom);
    const [address, setAddress] = useState("");
    const [googleMapsLoaded, setGoogleMapsLoaded] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [modalContent, setModalContent] = useState('');
    const searchOptions = {
        componentRestrictions: {country: 'id'},
    };
    function handleChangeLocation(lat, lng) {
        const apiKey = 'AIzaSyAuyS1LLibOZOGt-eliwsfzzTSYb3fVkmQ';
        const geocodingApiUrl = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${apiKey}`;

        setLocation({lat: lat, lng: lng});

        axios
            .get(geocodingApiUrl)
            .then(response => {
                const address = response.data.results[0]?.formatted_address || "";
                setAddress(address);
                form.setFieldsValue({latitude: lat, longitude: lng, address: address});
            })
            .catch(error => {
                console.error('Error fetching address:', error);
            });
    }

    function handleChangeZoom(newZoom) {
        setZoom(newZoom);
    }

    useEffect(() => {
        // Load Google Maps API script
        const googleMapsScript = document.createElement('script');
        googleMapsScript.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyAuyS1LLibOZOGt-eliwsfzzTSYb3fVkmQ&libraries=places`;
        googleMapsScript.async = true;
        googleMapsScript.onload = handleGoogleMapsLoad;
        document.body.appendChild(googleMapsScript);

        // Cleanup script
        return () => {
            document.body.removeChild(googleMapsScript);
        };
    }, []);

    const handleGoogleMapsLoad = () => {
        setGoogleMapsLoaded(true);
    };


    const handleSelect = async (address) => {
        setAddress(address);
        try {
            if (typeof address === 'string') {
                const results = await geocodeByAddress(address);
                const latLng = await getLatLng(results[0]);
                setLocation(latLng);
                form.setFieldsValue({latitude: latLng.lat, longitude: latLng.lng, address: address});
            }
        } catch (error) {
            console.error('Error while geocoding address:', error);
        }
    };

    const axiosInstance = axios.create({
        baseURL: 'http://back-admin.kilapin.com',
    });
    const {grossAmount, onGrossAmountChange, discountPrice, setDiscountPrice, initialPrice, selectedAddOns} = formElements.props;
    const onSubmit = (fieldsValue) => {
        const cleaner_balance = Math.round(initialPrice * 0.65); // Round the cleaner balance value
        const manipulatedFieldsValue = {
            ...fieldsValue,
            initial_price: initialPrice,
            gross_amount: grossAmount + 5000,
            total_discount: discountPrice,
            cleaner_balance: cleaner_balance,
            insurance : true,
            postal_code : "01",
            additional_service : selectedAddOns
        };

        // console.log(servicePrice)
        // Convert the time value to a string before submitting
        if (manipulatedFieldsValue.time && moment.isMoment(manipulatedFieldsValue.time)) {
            manipulatedFieldsValue.time = manipulatedFieldsValue.time.format("YYYY-MM-DD HH:mm");
        }

        console.log('field value',manipulatedFieldsValue)

        axiosInstance.post(`/api/order-booking/validate`, manipulatedFieldsValue)
            .then((response) => {
                console.log("table add on",response.data.success); // Log the fetched data
                if (response.data.success === true){
                    axios.post('https://customer.kilapin.com/order/input', manipulatedFieldsValue)
                    // axios.post('http://localhost:5002/order/input', manipulatedFieldsValue)
                        .then((res) => {
                            console.log('Post request successful:', res.data);
                            // Handle the response from the server as needed
                            message.success('Order Create Success!');
                            setModalContent(res.data.message);
                            setModalVisible(true);
                        })
                        .catch((error) => {
                            console.error('Error in POST request:', error);
                            message.error('Failed');
                            // Handle the error if the request fails
                        });
                }else{
                    message.error(`${response.data.message}`);
                }
            })
            .catch((error) => {
                console.log(error);
            });
        // console.log(manipulatedFieldsValue)
        // dispatch(crud.create(entity, manipulatedFieldsValue));

        // axios.post('https://customer.kilapin.com/order/input', manipulatedFieldsValue)
        //     .then((response) => {
        //         console.log('Post request successful:', response.data);
        //         // Handle the response from the server as needed
        //         message.success('Order Create Success!');
        //         setModalContent(response.data.message);
        //         setModalVisible(true);
        //     })
        //     .catch((error) => {
        //         console.error('Error in POST request:', error);
        //         message.error('Failed');
        //         // Handle the error if the request fails
        //     });
    };

    const closeModal = () => {
        setModalVisible(false);
        window.location.reload();
    };

    const copyToClipboard = () => {
        navigator.clipboard.writeText(modalContent)
            .then(() => {
                message.success('Copied to clipboard!');
            })
            .catch((error) => {
                console.error('Failed to copy: ', error);
                message.error('Failed to copy to clipboard.');
            });
    };

    useEffect(() => {
        if (isSuccess) {
            readBox.open();
            collapsedBox.open();
            panel.open();
            form.resetFields();
            dispatch(crud.resetAction("create"));
            dispatch(crud.list(entity));
        }
    }, [isSuccess]);

    return (
        <>
            <Modal
                visible={modalVisible}
                onCancel={closeModal}
                title="Response Message"
                footer={[
                    <Button key="copy" onClick={copyToClipboard}>
                        Copy to Clipboard
                    </Button>,
                    <Button key="close" onClick={closeModal}>
                        Close
                    </Button>
                ]}
            >
                <p>{modalContent}</p>
            </Modal>
        <Loading isLoading={isLoading}>
            {googleMapsLoaded ? ( // Check if both Google Maps and Places scripts are loaded
                <Form form={form} layout="vertical" onFinish={onSubmit}>
                    <Form.Item
                        hidden={true}
                        name="address" label="Address"
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        hidden={true}
                        name="latitude" label="Latitude"
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        hidden={true}
                        name="longitude" label="Longitude"
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item label="Search Places">
                        <PlacesAutocomplete value={address} onChange={setAddress} onSelect={handleSelect}
                                            searchOptions={searchOptions}>
                            {({getInputProps, suggestions, getSuggestionItemProps, loading}) => (
                                <div>
                                    <Input
                                        {...getInputProps({placeholder: 'Search Places...'})}
                                        size="large"
                                    />
                                    <div
                                        style={{
                                            position: 'absolute',
                                            width: '100%',
                                            maxHeight: '200px',
                                            overflowY: 'auto',
                                            borderRadius: 3,
                                            backgroundColor: '#fff',
                                            zIndex: 10,
                                        }}
                                    >
                                        {loading && (
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    padding: '10px',
                                                }}
                                            >
                                                <Spin indicator={<LoadingOutlined style={{fontSize: 24}} spin/>}/>
                                            </div>
                                        )}
                                        {suggestions.map((suggestion, index) => (
                                            <div
                                                {...getSuggestionItemProps(suggestion, {key: index})}
                                                style={{padding: '10px', cursor: 'pointer'}}
                                            >
                                                {suggestion.description}
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            )}
                        </PlacesAutocomplete>
                    </Form.Item>
                    <Form.Item label="Choose Location" style={{marginBottom: 20}}>
                        <MapPicker defaultLocation={location}
                                   zoom={zoom}
                                   mapTypeId="roadmap"
                                   style={{height: '300px'}}
                                   onChangeLocation={handleChangeLocation}
                                   onChangeZoom={handleChangeZoom}
                                   apiKey="AIzaSyAuyS1LLibOZOGt-eliwsfzzTSYb3fVkmQ"
                        />
                    </Form.Item>

                    {formElements}
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            ) : (
                <div>Loading Google Maps...</div>
            )}
        </Loading>
    </>
    );
}