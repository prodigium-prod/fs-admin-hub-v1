import React, {useCallback, useEffect, useState} from "react";
import {Button, Dropdown, Image, message, Modal, PageHeader, Table} from "antd";
import {EllipsisOutlined, ExclamationCircleOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import {selectListItems} from "@/redux/crud/selectors";
import uniqueId from "@/utils/uinqueId";
import axios from 'axios';
import { v4 as uuid } from 'uuid';

const {confirm} = Modal;


export default function DataTable({config, DropDownRowMenu, AddNewItem}) {
    let {entity, dataTableColumns, dataTableTitle} = config;
    dataTableColumns = dataTableColumns.map((column) => {
        if (column.dataIndex === "image") {
            return {
                ...column, render: (image) => <Image src={image} width={100}/>,
            };
        }
        return column;
    });

    if (entity === "order") {
        dataTableColumns.push({
            title: "Done Button",
            dataIndex: "Done",
            render: (text, record) =>
                record.status !== "Done" ? ( // Adjust the condition based on your order status property
                    <Button onClick={() => showDoneConfirmation(record)}>Done</Button>
                ) : (
                    <span></span>
                ),
        });
    }

    if (entity === "voucher") {
        dataTableColumns.push({
            title: "Generate Voucher",
            dataIndex: "generate",
            render: (text, record) => record.claimed >= record.maxClaims
                ? <Button disabled>Generate</Button>
                : record.canBeClaimed === 0 
                    ? <Button onClick={() => showGenerateConfirmation(record)}>Generate</Button>
                    : <span>Claimable</span>,
        });
    }

    dataTableColumns.push({
        title: "", render: (row) => (<Dropdown overlay={DropDownRowMenu({row})} trigger={["click"]}>
            <EllipsisOutlined style={{cursor: "pointer", fontSize: "24px"}}/>
        </Dropdown>),
    });

    const handelDataTableLoad = useCallback((pagination) => {
        // console.log('disini masuk')
        dispatch(crud.list(entity, pagination.current));
    }, []);

    useEffect(() => {
        // console.log('disini masuk')
        dispatch(crud.list(entity));
    }, []);

    const {result: listResult, isLoading: listIsLoading} = useSelector(selectListItems);

    const [userFinding, setUserFinding] = useState([]);

    const {pagination, items} = listResult;

    const dispatch = useDispatch();
    const axiosInstance = axios.create({
        baseURL: 'http://localhost:8888',
    });

    useEffect(() => {
        // Fetch hotel names from API using userId
        axiosInstance.get(`/api/users/list`)
            .then((response) => {
                console.log(response.data); // Log the fetched data
                setUserFinding(response.data.result);
            })
            .catch((error) => {
                console.log(error);
            });
    }, []);

    function handleDoneOrder(record) {
        axios.post('http://back-admin.kilapin.com/api/order-booking/status', {order_id : record.order_id, status: "Done"})
        //     axios.post('http://localhost:8888/api/order-booking/status', {order_id : record.order_id, status: "Done"})
            .then((res) => {
                console.log('Post request successful:', res.data);
                // Handle the response from the server as needed
                message.success('Order has been done!');
            })
            .catch((error) => {
                console.error('Error in POST request:', error);
                message.error('Failed');
            });
    }

    const handleGenerateVoucher = (record) => {
        if (record.claimed <= record.maxClaims){
            userFinding.forEach(user => {
                let idVoucher = uuid()
                const newVoucher = {
                    // Create a new object for the voucher based on the record
                    id : idVoucher,
                    zone : record.zone,
                    code: record.code,
                    discount: record.discount,
                    type: record.type,
                    service: record.service,
                    membership: record.membership,
                    event: record.event,
                    transMinimum: record.transMinimum,
                    maxDiscount: record.maxDiscount,
                    maxClaims: record.maxClaims,
                    claimed: record.claimed,
                    validFrom: record.validFrom,
                    validUntil: record.validUntil,
                    maxUserUsed: record.maxUserUsed,
                    voucherUsed: record.voucherUsed,
                    tnc: record.tnc,
                    image: record.image,
                };
                console.log(user)
                if (record.membership === "All"){
                    user.vouchers.push(newVoucher);
                    console.log(user)
                    dispatch(crud.update('users', user._id, user));

                } else{
                    if (user.member === record.membership) {
                        user.vouchers.push(newVoucher);
                        console.log(user)
                        dispatch(crud.update('users', user._id, user));
                    }else{
                        console.log("nothing found membership")
                    }
                }
            });

            record.claimed += 1

            dispatch(crud.update('voucher', record._id, record));
        }else {
            alert("max claim")
        }

    };

    const showGenerateConfirmation = (record) => {
        confirm({
            title: "Generate Voucher",
            icon: <ExclamationCircleOutlined/>,
            content: "Are you sure you want to generate the voucher?",
            onOk: () => handleGenerateVoucher(record),
        });
    };

    const showDoneConfirmation = (record) => {
        confirm({
            title: "Confirmation order done",
            icon: <ExclamationCircleOutlined/>,
            content: "Order is done?",
            onOk: () => handleDoneOrder(record),
        });
    };

    return (<>
        <PageHeader
            onBack={() => window.history.back()}
            title={dataTableTitle}
            ghost={false}
            extra={[<Button onClick={handelDataTableLoad} key={`${uniqueId()}`}>
                Refresh
            </Button>, <AddNewItem key={`${uniqueId()}`} config={config}/>,]}
            style={{
                padding: "20px 0px",
            }}
        ></PageHeader>
        <Table
            columns={dataTableColumns}
            rowKey={(item) => item._id}
            dataSource={items}
            pagination={pagination}
            loading={listIsLoading}
            onChange={handelDataTableLoad}
        />
    </>);
}