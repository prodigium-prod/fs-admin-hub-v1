import React, {useState} from "react";

import {Link} from "react-router-dom";
import {Layout, Menu} from "antd";
import {DashboardOutlined, FileTextOutlined, SettingOutlined, TeamOutlined,} from "@ant-design/icons";
import {useSelector} from "react-redux";
import {selectAuth} from "@/redux/auth/selectors";

const {Sider} = Layout;
const {SubMenu} = Menu;

function Navigation() {
    const [collapsed, setCollapsed] = useState(false);
    const {current, isLoading, isSuccess} = useSelector(selectAuth);

    console.log("IKI CURRENT : ", current.role)
    const onCollapse = () => {
        setCollapsed(!collapsed);
    };
    return (
        <>
            <Sider
                collapsible
                collapsed={collapsed}
                onCollapse={onCollapse}
                style={{
                    zIndex: 1000,
                }}
            >
                <div className="logo"/>
                <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
                    <Menu.Item key="1" icon={<DashboardOutlined/>}>
                        <Link to="/"/>
                        Home Page
                    </Menu.Item>
                    {current.role === "Admin Hub" && ( // Add the conditional rendering logic here
                        <>
                            <Menu.Item key="21" icon={<FileTextOutlined/>}>
                                <Link to="/order">Order Assign</Link>
                            </Menu.Item>
                            <Menu.Item key="29" icon={<FileTextOutlined/>}>
                                <Link to="/order-booking">Create Order</Link>
                            </Menu.Item>
                            <Menu.Item key="27" icon={<FileTextOutlined/>}>
                                <Link to="/tracking-cleaner">Tracking Cleaner</Link>
                            </Menu.Item>
                            <Menu.Item key="28" icon={<FileTextOutlined/>}>
                                <Link to="/users-additional">Add Customer</Link>
                            </Menu.Item>
                        </>


                    )}
                    {current.role === "Admin" && (
                        <>
                            <Menu.Item key="22" icon={<FileTextOutlined/>}>
                                <Link to="/news">News</Link>
                            </Menu.Item>
                            <Menu.Item key="23" icon={<FileTextOutlined/>}>
                                <Link to="/membership">Membership</Link>
                            </Menu.Item>
                            <Menu.Item key="24" icon={<FileTextOutlined/>}>
                                <Link to="/voucher">Voucher</Link>
                            </Menu.Item>
                            <Menu.Item key="25" icon={<FileTextOutlined/>}>
                                <Link to="/category">Category Service</Link>
                            </Menu.Item>
                            <Menu.Item key="26" icon={<FileTextOutlined/>}>
                                <Link to="/additional-service">Additional Service</Link>
                            </Menu.Item>
                            <Menu.Item key="31" icon={<TeamOutlined/>}>
                                <Link to="/admin"/>
                                Admins Management
                            </Menu.Item>
                            <Menu.Item key="32" icon={<SettingOutlined/>}>
                                <Link to="/settings"/>
                                Settings
                            </Menu.Item>
                        </>
                    )}
                </Menu>
            </Sider>
        </>
    );
}

export default Navigation;