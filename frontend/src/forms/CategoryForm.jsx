import { Form, Input, } from "antd";
import {  useSelector } from "react-redux";
import { useCrudContext } from "@/context/crud";
import { selectUpdatedItem } from "@/redux/crud/selectors";


export default function CategoryForm({ isUpdateForm = false, keyState }) {
    const { current, isLoading, isSuccess } = useSelector(selectUpdatedItem);
    const { state, crudContextAction } = useCrudContext();


    return (
        <>
            <Form.Item
                label="name"
                name="name"
                rules={[
                    {
                        required: true,
                        message: "Please input your category name!",
                    },
                ]}
            >
                <Input />
            </Form.Item>
        </>
    );
}
