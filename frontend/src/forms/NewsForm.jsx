import {Form, Input, Select} from "antd";
import {useSelector} from "react-redux";
import {useCrudContext} from "@/context/crud";
import {selectUpdatedItem} from "@/redux/crud/selectors";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import {useState} from "react";


export default function NewsForm({isUpdateForm = false, keyState, uploadFormItemProps}) {
    const {current, isLoading, isSuccess} = useSelector(selectUpdatedItem);
    const {crudContextAction} = useCrudContext();
    const [htmlContent, setHtmlContent] = useState("");


    return (
        <>
            <Form.Item
                label="Title"
                name="title"
                rules={[
                    {
                        required: true,
                        message: "Please input the title!",
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Desc"
                name="desc"
                rules={[
                    {
                        required: true,
                        message: "Please input the description!",
                    },
                ]}
            >
                <ReactQuill
                    value={htmlContent}
                    onChange={setHtmlContent}
                />
            </Form.Item>
            <Form.Item
                label="Status"
                name="status"
                rules={[
                    {
                        required: true,
                        message: "Please select the status!",
                    },
                ]}
                initialValue={current?.status} // Set the initial value based on the 'current' object
            >
                <Select>
                    <Select.Option value={"Active"}>Active</Select.Option>
                    <Select.Option value={"Inactive"}>Inactive</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item
                label="lang"
                name="lang"
                rules={[
                    {
                        required: true,
                        message: "Please select the status!",
                    },
                ]}
                initialValue={current?.lang} // Set the initial value based on the 'current' object
            >
                <Select>
                    <Select.Option value={"English"}>English</Select.Option>
                    <Select.Option value={"Indonesia"}>Indonesia</Select.Option>
                </Select>
            </Form.Item>
        </>
    );
}
