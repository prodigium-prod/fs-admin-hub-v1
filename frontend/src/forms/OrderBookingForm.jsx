import React, {useEffect, useState} from "react";

import {Button, Col, DatePicker, Form, Input, message, Radio, Row, Select, TreeSelect} from "antd";
import {useSelector} from "react-redux";
import {useCrudContext} from "@/context/crud";
import {selectUpdatedItem} from "@/redux/crud/selectors";
import axios from 'axios';

const {Option} = Select;

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8888',
});


export default function OrderBookingForm({isUpdateForm = false, keyState,selectedAddOns, setSelectedAddons,initialPrice, setInitialPrice, onGrossAmountChange, grossAmount, discountPrice, setDiscountPrice}) {

    const {current, isLoading, isSuccess} = useSelector(selectUpdatedItem);
    const {state, crudContextAction} = useCrudContext();
    const [serviceNames, setServiceNames] = useState([]);
    const [customerNames, setCustomerNames] = useState([]);
    const [voucher, setVoucher] = useState([]);
    const [addOns, setAddOns] = useState([]);
    const [selectedCustomerId, setSelectedCustomerId] = useState("");
    const [selectedServiceId, setSelectedServiceId] = useState("");
    const [selectedVoucherCode, setSelectedVoucherCode] = useState({});
    const [checkedValue, setCheckedValue] = useState("Nothing");
    const [unitQuantities, setUnitQuantities] = useState({});
    const [addOnsPrice, setAddOnsPrice] = useState(0);
    const [servicePrice, setServicePrice] = useState(0);


    useEffect(() => {
        // Fetch hotel names from API using userId
        axiosInstance.get(`/api/service/list/${checkedValue}`)
            .then((response) => {
                console.log('test', response.data); // Log the fetched data
                setServiceNames(response.data.result);
            })
            .catch((error) => {
                console.log(error);
            });

    }, [checkedValue]);

    useEffect(() => {
        // Fetch hotel names from API using userId

        axiosInstance.get(`/api/users-additional/list`)
            .then((response) => {
                // console.log(response.data); // Log the fetched data
                setCustomerNames(response.data.result);
            })
            .catch((error) => {
                console.log(error);
            });

        axiosInstance.get(`/api/voucher/list`)
            .then((response) => {
                // console.log("table voucher",response.data); // Log the fetched data
                setVoucher(response.data.result);
            })
            .catch((error) => {
                console.log(error);
            });

        axiosInstance.get(`/api/add-on/list`)
            .then((response) => {
                console.log("table add on",response.data); // Log the fetched data
                setAddOns(response.data.result);
            })
            .catch((error) => {
                console.log(error);
            });

    }, []);


    const handleCustomerSelect = (value) => {
        const selectedCleaner = customerNames.find((cleaner) => cleaner.name === value);
        if (selectedCleaner) {
            console.log(selectedCleaner._id)
            setSelectedCustomerId(selectedCleaner._id);
        }
    };

    const handleServiceSelect = (value) => {
        const selectedCleaner = serviceNames.find((cleaner) => cleaner._id === value);
        if (selectedCleaner) {
            setSelectedServiceId(selectedCleaner._id);
            let amount = parseInt(selectedCleaner.price);
            setServicePrice(amount);
        }
    };


    // const [selectedVoucher, setSelectedVoucher] = useState(''); // Initialize with an empty string
    const handleVoucherSelect = (value) => {
        let selectedVoucher = voucher.find((data) => data.code === value);

        if (selectedVoucher) {
            setSelectedVoucherCode(selectedVoucher)
        }else{
            setSelectedVoucherCode({})
        }
    };

    const handleAddOnsSelect = (values) => {

        const selectedAddonsData = values.map((value) =>
            addOns.find((addon) => addon._id === value)
        );

        setSelectedAddons(selectedAddonsData);
        console.log(selectedAddOns)
    };

    const handleUnitQuantityChange = (addonId, quantity) => {
        setUnitQuantities((prevQuantities) => ({
            ...prevQuantities,
            [addonId]: quantity,
        }));

        console.log("Unit Qty",unitQuantities)
    };

    const handleUnitQuantitySubmit = (addonId) => {
        console.log(`Quantity by Id ${addonId}: ${unitQuantities[addonId]}`);
        const selectedAddOnIndex = selectedAddOns.findIndex((data) => data._id === addonId);

        if (selectedAddOnIndex !== -1) {
            const updatedSelectedAddOns = [...selectedAddOns]; // Create a copy of the array
            updatedSelectedAddOns[selectedAddOnIndex] = {
                ...updatedSelectedAddOns[selectedAddOnIndex], // Create a copy of the selected add-on object
                total: unitQuantities[addonId], // Update the unitQuantity property
            };

            setSelectedAddons(updatedSelectedAddOns); // Update the state with the modified array
        }
    };


    useEffect(() => {

        let totalPrice = selectedAddOns.reduce((total, addon) => {
            return total + parseInt(addon.total) * addon.price;
        }, 0);

        setAddOnsPrice(totalPrice);
        let grandTotal = servicePrice + addOnsPrice
        setInitialPrice(grandTotal)
        if (Object.keys(selectedVoucherCode).length !== 0){
            console.log(selectedVoucherCode)
            if(grandTotal > selectedVoucherCode.transMinimum){
                const currentDate = new Date();
                const validFromDate = new Date(selectedVoucherCode.validFrom);
                const validUntilDate = new Date(selectedVoucherCode.validUntil);
                let totalDisc
                if (currentDate >= validFromDate && currentDate <= validUntilDate) {
                    if (selectedVoucherCode.onlyPrice === "1"){
                        console.log('masuk')
                        totalDisc = grandTotal - selectedVoucherCode.maxDiscount
                        grandTotal = selectedVoucherCode.maxDiscount
                        setDiscountPrice(totalDisc)
                    }else{
                        totalDisc = (grandTotal * selectedVoucherCode.discount) / 100;
                        if (totalDisc > selectedVoucherCode.maxDiscount){
                            grandTotal -= selectedVoucherCode.maxDiscount
                            setDiscountPrice(selectedVoucherCode.maxDiscount)
                        }else {
                            grandTotal -= totalDisc;
                            setDiscountPrice(totalDisc)
                        }
                    }
                } else {
                    message.error("Voucher is not valid date")
                }
            }else{
                message.error(`Transaction minimum Rp.${selectedVoucherCode.transMinimum ? selectedVoucherCode.transMinimum.toLocaleString('id-ID') : 0}`);
            }
        }
        onGrossAmountChange(grandTotal)

    }, [selectedAddOns, unitQuantities, addOnsPrice, grossAmount, initialPrice, selectedVoucherCode, servicePrice]);


    const handleRadioChange = (e) => {
        const value = e.target.value;
        setCheckedValue(value)
        console.log(value)
    };


    // let selectedVoucher;
    return (<>
            <Form.Item
                hidden={true}
                label="booking_type"
                name="booking_type"
                initialValue={"Booking"}
                rules={[{
                    message: "Please input your member!",
                },]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                hidden={true}
                label="category"
                name="category"
                initialValue={"General Cleaning"}
                rules={[{
                    message: "Please input your member!",
                },]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Customer"
                name="customer_id"
                rules={[{
                    required: true, message: "Please input your manager name!",
                },]}
            >
                <Select onChange={handleCustomerSelect}>
                    {customerNames.map((cleaner) => (<Select.Option key={cleaner._id} value={cleaner._id}>
                            {/* {`cleaner.name`} */}
                            {`${cleaner.name} - ${cleaner.phone}`}
                        </Select.Option>))}
                </Select>
                {/* <Input /> */}
            </Form.Item>
            <Form.Item
                label="Select Date and Time"
                name="time"
                rules={[{
                    required: true, message: 'Please select a date and time!',
                },]}
            >
                <DatePicker
                    showTime
                    format="YYYY-MM-DD HH:mm:ss"
                />
            </Form.Item>

            <Form.Item label="Type Floor" style={{justifyContent: "center"}}>
                <Radio.Group buttonStyle="solid" defaultValue={checkedValue} onChange={handleRadioChange}
                             style={{alignItems: "center"}}>
                    <Row style={{width: 400}}>
                        <Col span={12}>
                            <Radio.Button value="Granite">Granite</Radio.Button>
                        </Col>
                        <Col span={12}>
                            <Radio.Button value="Marmer">Marmer</Radio.Button>
                        </Col>
                    </Row>
                    <Row style={{width: 400}}>
                        <Col span={12}>
                            <Radio.Button value="Granite and Marmer">Granite and Marmer</Radio.Button>
                        </Col>
                        <Col span={12}>
                            <Radio.Button value="Nothing">Nothing</Radio.Button>
                        </Col>
                    </Row>
                </Radio.Group>
            </Form.Item>
            <Form.Item
                label="Size"
                name="service_id"
                rules={[{
                    required: true, message: "Please input your manager name!",
                },]}
            >
                <Select onChange={handleServiceSelect}>
                    {serviceNames.map((cleaner) => (
                        <Select.Option key={cleaner._id} value={cleaner._id}>
                            {/* {`cleaner.name`} */}
                            {`${cleaner.description} - ${cleaner.option}`}
                        </Select.Option>
                    ))}
                </Select>
            </Form.Item>
            <Form.Item
                label="Additional Services"
                name="additional_service"
                rules={[
                    {
                        // required: true, // Indicates the field is required
                        message: 'Please select additional services!', // Custom error message
                        type: 'array', // Specifies that the value should be an array (for multiple selection)
                    },
                ]}
            >

                <Select onChange={handleAddOnsSelect}  mode="tags" allowClear>
                    {addOns.map((data) => (
                        <Select.Option key={data._id} value={data._id}>
                        {`${data.type} - ${data.type_option ? data.type_option : ""} - ${data.type_class ? data.type_class : ""}`}
                        </Select.Option>
                    ))}
                </Select>
                {selectedAddOns.map((addon) =>
                    ['Sofa', 'Carpet Cleaning', 'Storage Cleaning and Tidying'].includes(addon.type) ? (
                        <Form.Item key={addon._id} label={`Unit for ${addon.type} ${addon.type_option ? addon.type_option : ""}`} style={{marginTop:10}}>
                            <Input
                                value={unitQuantities[addon._id] || ''}
                                onChange={(e) =>
                                    handleUnitQuantityChange(addon._id, e.target.value)
                                }
                            />
                            <Button onClick={() => handleUnitQuantitySubmit(addon._id)}>
                                Submit
                            </Button>
                        </Form.Item>
                    ) : null
                )}

            </Form.Item>

            <Form.Item
                label="Voucher"
                name="voucher"
                rules={[{
                    message: "Please input your manager name!",
                    type: "string"
                },]}
            >
                <Select onChange={handleVoucherSelect}>
                    <Select.Option key={0} value={"NO"}>
                        NO
                    </Select.Option>
                    {voucher.map((data) => (
                            <Select.Option key={data._id} value={data.code}>
                                {`${data.code} - ${data.discount}%`}
                            </Select.Option>
                    ))}
                </Select>
            </Form.Item>
            <Form.Item
                label="Notes"
                name="notes"
                rules={[{
                    message: "Please input your notes!",
                },]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Initial Amount"
                name="initial_price"
                rules={[{
                    message: "Please input your member!",
                },]}
            >
                <Input
                    placeholder={`IDR ${initialPrice.toLocaleString('id-ID')}`} value={initialPrice}
                    readOnly
                />
            </Form.Item>
            <Form.Item
                label="Discount"
                name="discount"
                rules={[{
                    message: "Please input your member!",
                },]}
            >
                <Input
                    placeholder={`IDR ${discountPrice.toLocaleString('id-ID')}`} value={discountPrice}
                    readOnly
                />
            </Form.Item>
            <Form.Item
                label="Amount"
                name="gross_amount"
                rules={[{
                    message: "Please input your member!",
                },]}
            >
                <Input
                    placeholder={`IDR ${grossAmount.toLocaleString('id-ID')}`} value={grossAmount}
                    readOnly
                />
            </Form.Item>
        </>);
}