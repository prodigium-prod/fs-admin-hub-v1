import React, {useEffect, useState} from "react";

import {AutoComplete, Form, Input, Select} from "antd";
import {useSelector} from "react-redux";
import {useCrudContext} from "@/context/crud";
import {selectUpdatedItem} from "@/redux/crud/selectors";
import axios from 'axios';

const {Option} = Select;

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8888',
});


export default function VoucherForm({isUpdateForm = false, keyState}) {
    const {current, isLoading, isSuccess} = useSelector(selectUpdatedItem);
    const {state, crudContextAction} = useCrudContext();
    const [membershipNames, setMembershipNames] = useState([]);
    const [categoryNames, setCategoryNames] = useState([]);
    const [selectedIdMember, setSelectedIdMember] = useState("");
    const [selectedIdCategory, setSelectedIdCategory] = useState("");
    const [showClaimDates, setShowClaimDates] = useState(false);


    useEffect(() => {
        // Fetch hotel names from API using userId
        axiosInstance.get(`/api/membership/list`)
            .then((response) => {
                console.log(response.data); // Log the fetched data
                setMembershipNames(response.data.result);
            })
            .catch((error) => {
                console.log(error);
            });
        axiosInstance.get(`/api/category/list`)
            .then((response) => {
                console.log(response.data); // Log the fetched data
                setCategoryNames(response.data.result);
            })
            .catch((error) => {
                console.log(error);
            });
    }, []);

    const handleCanBeClaimedSelect = (value) => {
        setShowClaimDates(value === 1); // Show claim dates if canBeClaimed is 'Yes' (value 1)
    };


    const handleMembershipSelect = (value) => {
        let selectedMember = membershipNames.find((e) => e.name === value);
        if (selectedMember) {
            setSelectedIdMember(selectedMember._id);
        }
    };
    const handleCategorySelect = (value) => {
        let selectedCategory = categoryNames.find((e) => e.name === value);
        if (selectedCategory) {
            setSelectedIdCategory(selectedCategory._id);
        }
    };

    return (
        <>
            <Form.Item
                label="Type"
                name="type"
                rules={[
                    {
                        required: true,
                        message: "Please select the status!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingLeft: "5px",
                }}
                initialValue={current?.type} // Set the initial value based on the 'current' object
            >
                <Select>
                    <Select.Option value={"Urgent"}>Urgent</Select.Option>
                    <Select.Option value={"Booking"}>Booking</Select.Option>
                    <Select.Option value={"All"}>All</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item
                label="Service"
                name="service"
                rules={[
                    {
                        required: true,
                        message: "Please select the status!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingLeft: "5px",
                }}
            >
                <Select onChange={handleCategorySelect}>
                    {categoryNames.map((membership) => (
                        <Select.Option key={membership._id} value={membership.name}>
                            {`${membership.name}`}
                        </Select.Option>
                    ))}
                    <Select.Option value={'All'}>
                        {`All`}
                    </Select.Option>
                </Select>
            </Form.Item>
            <Form.Item
                label="Zone"
                name="zone"
                rules={[
                    {
                        required: true,
                        message: "Please input the subdistrict!",
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Event"
                name="event"
                rules={[
                    {
                        required: true,
                        message: "Please input your address!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingLeft: "5px",
                }}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Code"
                name="code"
                rules={[
                    {
                        required: true,
                        message: "Please input your time!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingRight: "5px",
                }}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="transMinimum"
                name="transMinimum"
                rules={[
                    {
                        required: true,
                        message: "Please input your address!",
                    },
                ]}
            >
                <Input type={"number"}/>
            </Form.Item>
            <Form.Item
                label="Discount"
                name="discount"
                rules={[
                    {
                        required: true,
                        message: "Please input your address!",
                    },
                ]}
            >
                <Input type={"number"}/>
            </Form.Item>
            <Form.Item
                label="maxDiscount / Only Price"
                name="maxDiscount"
                rules={[
                    {
                        required: true,
                        message: "Please input your address!",
                    },
                ]}
            >
                <Input type={"number"}/>
            </Form.Item>
            <Form.Item
                label="onlyPrice"
                name="onlyPrice"
                rules={[
                    {
                        required: true,
                        message: "Please select the status!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingRight: "5px",
                }}
            >
                <Select>
                    <Select.Option value={1}>Yes</Select.Option>
                    <Select.Option value={0}>No</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item
                label="Membership"
                name="membership"
                rules={[
                    {
                        required: true,
                        message: "Please input your manager name!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingRight: "5px",
                }}
            >
                <Select onChange={handleMembershipSelect}>
                    {membershipNames.map((membership) => (
                        <Select.Option key={membership._id} value={membership.name}>
                            {`${membership.name}`}
                        </Select.Option>
                    ))}
                    <Select.Option value={'All'}>
                        {`All`}
                    </Select.Option>
                </Select>
            </Form.Item>
            <Form.Item
                label="maxUserUsed"
                name="maxUserUsed"
                rules={[
                    {
                        required: true,
                        message: "Please input your time!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingRight: "5px",
                }}
            >
                <Input type={"number"}/>
            </Form.Item>
            <Form.Item
                label="validFrom"
                name="validFrom"
                rules={[
                    {
                        required: true,
                        message: "Please select a date!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingRight: "5px",
                }}
            >
                <Input type={"date"}/>
            </Form.Item>
            <Form.Item
                label="validUntil"
                name="validUntil"
                rules={[
                    {
                        required: true,
                        message: "Please select a date!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingLeft: "5px",
                }}
            >
                <Input type={"date"}/>
            </Form.Item>
            <Form.Item
                label="maxClaims / maxGenerate"
                name="maxClaims"
                rules={[
                    {
                        required: true,
                        message: "Please input your time!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingRight: "5px",
                }}
            >
                <Input type={"number"}/>
            </Form.Item>
            <Form.Item
                label="canBeClaimed"
                name="canBeClaimed"
                rules={[
                    {
                        required: true,
                        message: "Please select the status!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingRight: "5px",
                }}
            >
                <Select onChange={handleCanBeClaimedSelect}>
                    <Select.Option value={1}>Yes</Select.Option>
                    <Select.Option value={0}>No</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item
                label="tnc"
                name="tnc"
                rules={[
                    {
                        required: true,
                        message: "Please input your address!",
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            {showClaimDates && ( // Show claimFrom and claimUntil fields if showClaimDates is true
                <>
                    <Form.Item
                        label="claimFrom"
                        name="claimFrom"
                        style={{
                            display: "inline-block",
                            width: "calc(50%)",
                            paddingRight: "5px",
                        }}
                    >
                        <Input type={"date"}/>
                    </Form.Item>
                    <Form.Item
                        label="claimUntil"
                        name="claimUntil"
                        style={{
                            display: "inline-block",
                            width: "calc(50%)",
                            paddingLeft: "5px",
                        }}
                    >
                        <Input type={"date"}/>
                    </Form.Item>
                </>
            )}
        </>
    );
}