import React, { useState, useEffect } from "react";

import { Button, Form, Input, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { crud } from "@/redux/crud/actions";
import { useCrudContext } from "@/context/crud";
import { selectUpdatedItem } from "@/redux/crud/selectors";
import axios from 'axios';
const { Option } = Select;

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8888',
});

export default function AdditionalServiceForm({ isUpdateForm = false, keyState }) {
    const {current, isLoading, isSuccess} = useSelector(selectUpdatedItem);
    const {state, crudContextAction} = useCrudContext();
    const [categoryNames, setCategoryNames] = useState([]);
    const [selectedIdMember, setSelectedIdMember] = useState("");
    const [selectedIdCategory, setSelectedIdCategory] = useState("");
    const [showClaimDates, setShowClaimDates] = useState(false);
    const [type, setType] = useState('')
    const [typeOption, setTypeOption] = useState('')


    useEffect(() => {
        // Fetch hotel names from API using userId
        axiosInstance.get(`/api/category/list`)
            .then((response) => {
                console.log(response.data); // Log the fetched data
                setCategoryNames(response.data.result);
            })
            .catch((error) => {
                console.log(error);
            });
    }, []);


    const handleCategorySelect = (value) => {
        setType('')
        setTypeOption('')
        console.log(value)
        switch (value) {
            case "GENERAL CLEANING" :
                setType('Type')
                break
            case "AIR CONDITIONER" :
                setTypeOption('Size AC/PK')
                setType('Type Service')
                break
        }
        let selectedCategory = categoryNames.find((e) => e.name === value);
        if (selectedCategory) {
            setSelectedIdCategory(selectedCategory._id);
        }
    };


    return (
        <>
            <Form.Item
                label="Category Service"
                name="category"
                rules={[
                    {
                        required: true,
                        message: "Please select the Category!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingLeft: "5px",
                }}
            >
                <Select onChange={handleCategorySelect}>
                    {categoryNames.map((category) => (
                        <Select.Option key={category._id} value={category.name}>
                            {`${category.name}`}
                        </Select.Option>
                    ))}
                </Select>
            </Form.Item>

            <Form.Item
                label={type ? type : "Type"}
                name="type"
                rules={[
                    {
                        required: true,
                        message: "Please input the type!",
                    },
                ]}
                style={{
                    display: "inline-block",
                    width: "calc(50%)",
                    paddingLeft: "5px",
                }}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label={typeOption ? typeOption : "Type Option"}
                name="type_option"
                rules={[
                    {
                        message: "Please input the type option!",
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label={"Type Class"}
                name="type_class"
                rules={[
                    {
                        message: "Please input the type class!",
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label={"Unit"}
                name="unit"
                rules={[
                    {
                        message: "Please input the unit meter or something!",
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label={"Price"}
                name="price"
                rules={[
                    {
                        required: true,
                        message: "Please input the type option!",
                    },
                ]}
            >
                <Input type={"number"}/>
            </Form.Item>

        </>
    );
}