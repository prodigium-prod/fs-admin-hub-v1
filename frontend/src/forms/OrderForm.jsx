import React, { useState, useEffect } from "react";

import { Button, Form, Input, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { crud } from "@/redux/crud/actions";
import { useCrudContext } from "@/context/crud";
import { selectUpdatedItem } from "@/redux/crud/selectors";
import axios from 'axios';
const { Option } = Select;

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8888',
  });

export default function OrderForm({ isUpdateForm = false, keyState }) {
  const { current, isLoading, isSuccess } = useSelector(selectUpdatedItem);
  const { state, crudContextAction } = useCrudContext();
  const [cleanerNames, setCleanerNames] = useState([]);
  const [selectedHotelId, setSelectedCleanerId] = useState("");

  useEffect(() => {
    // Fetch hotel names from API using userId
    axiosInstance.get(`/api/cleaner/list`)
      .then((response) => {
        console.log(response.data); // Log the fetched data
        setCleanerNames(response.data.result);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const handleCleanerSelect = (value) => {
    const selectedCleaner = cleanerNames.find((cleaner) => cleaner.name === value);
    if (selectedCleaner) {
      setSelectedCleanerId(selectedCleaner._id);
    }
  };

  return (
    <>
      <Form.Item
        label="Address"
        name="address"
        rules={[
          {
            required: true,
            message: "Please input your address!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Time"
        name="time"
        rules={[
          {
            required: true,
            message: "Please input your time!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Cleaner"
        name="cleaner_id"
        rules={[

          {
            // required: true,
            message: "Please input your manager name!",
          },
        ]}
      >
        <Select onChange={handleCleanerSelect}>
          {cleanerNames.map((cleaner) => (
            <Select.Option key={cleaner._id} value={cleaner._id}>
              {/* {`cleaner.name`} */}
              {`${cleaner.name} - ${cleaner.cleaner_detail.status}`}
            </Select.Option>
          ))}
        </Select>
        {/* <Input /> */}
      </Form.Item>

    </>
  );
}