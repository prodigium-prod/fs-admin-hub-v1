import React, { useState, useEffect } from "react";

import { Button, Form, Input, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { crud } from "@/redux/crud/actions";
import { useCrudContext } from "@/context/crud";
import { selectUpdatedItem } from "@/redux/crud/selectors";
import axios from 'axios';
const { Option } = Select;

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8888',
});

export default function UserAdditionalForm({ isUpdateForm = false, keyState }) {
    const { current, isLoading, isSuccess } = useSelector(selectUpdatedItem);
    const { state, crudContextAction } = useCrudContext();

    return (
        <>
            <Form.Item
                label="Name"
                name="name"
                rules={[
                    {
                        required: true,
                        message: "Please input your name!",
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="Email"
                name="email"
                rules={[
                    {
                        required: true,
                        message: "Please input your name!",
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="Phone"
                name="phone"
                rules={[
                    {
                        required: true,
                        message: "Please input your phone!",
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="Address"
                name="address"
                rules={[
                    {
                        required: true,
                        message: "Please input your address!",
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                hidden={true}
                label="role"
                name="role"
                initialValue={"Kilapin Customer"}
                rules={[
                    {
                        message: "Please input your address!",
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                hidden={true}
                label="member"
                name="member"
                initialValue={"Fresh"}
                rules={[
                    {
                        message: "Please input your member!",
                    },
                ]}
            >
                <Input  />
            </Form.Item>

        </>
    );
}