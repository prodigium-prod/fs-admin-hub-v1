import React, {useState} from "react";

import {Button, DatePicker, Menu, Modal} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import {selectItemById} from "@/redux/crud/selectors";
import {useCrudContext} from "@/context/crud";
import uniqueId from "@/utils/uinqueId";
import DataTableList from "@/modules/ListModule/DataTableList";
import axios from "axios";


function ExportModal({visible, onCancel, onExport}) {
    const [filterOption, setFilterOption] = useState({
        dateRange: [],
    });

    const handleExport = () => {
        // Perform any validation on filterOption if required
        onExport(filterOption); // Pass the filter option to the onExport function
        onCancel(); // Close the modal after exporting
    };

    const handleDateRangeChange = (dates) => {
        // dates will be an array containing [start date, end date]
        setFilterOption({
            ...filterOption,
            dateRange: dates,
        });
    };

    return (
        <Modal
            visible={visible}
            onCancel={onCancel}
            title="Export Data"
            footer={[
                <Button key="cancel" onClick={onCancel}>
                    Cancel
                </Button>,
                <Button key="export" type="primary" onClick={handleExport}>
                    Export
                </Button>,
            ]}
        >
            {/* Date Range filter */}
            <DatePicker.RangePicker
                value={filterOption.dateRange}
                onChange={handleDateRangeChange}
            />

            {/* Location Address filter */}
            {/*<Input*/}
            {/*    type="text"*/}
            {/*    value={filterOption.locationAddress}*/}
            {/*    onChange={handleLocationAddressChange}*/}
            {/*    placeholder="Enter Location Address"*/}
            {/*/>*/}
        </Modal>
    );
}


function ExportItem({config}) {
    const {EXPORT_ENTITY} = config;
    const [isExportModalVisible, setExportModalVisible] = useState(false);
    const axiosInstance = axios.create({
        baseURL: 'http://192.168.1.135',
    });
    const handleExport = (filterOption) => {
        console.log(filterOption)
        axiosInstance.post("/api/tracking-cleaner").then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log("Export failed:", error);
            });
    };

    const showModal = () => {
        setExportModalVisible(true);
    };

    const handleModalCancel = () => {
        setExportModalVisible(false);
    };

    return (
        <>
            <Button onClick={showModal} type="primary">
                {EXPORT_ENTITY}
            </Button>
            <ExportModal
                visible={isExportModalVisible}
                onCancel={handleModalCancel}
                onExport={handleExport}
            />
        </>
    );
}

function DropDownRowMenu({row}) {
    const dispatch = useDispatch();
    const {crudContextAction} = useCrudContext();
    const {panel, collapsedBox, modal, readBox, editBox} = crudContextAction;
    const item = useSelector(selectItemById(row._id));
    const Show = () => {
        dispatch(crud.currentItem(item));
        panel.open();
        collapsedBox.open();
        readBox.open();
    };

    function Edit() {
        dispatch(crud.currentAction("update", item));
        editBox.open();
        panel.open();
        collapsedBox.open();
    }

    function Delete() {
        dispatch(crud.currentAction("delete", item));
        modal.open();
    }

    return (
        <Menu style={{width: 130}}>
            <Menu.Item key={`${uniqueId()}`} icon={<EyeOutlined/>} onClick={Show}>
                Show
            </Menu.Item>
            <Menu.Item key={`${uniqueId()}`} icon={<EditOutlined/>} onClick={Edit}>
                Edit
            </Menu.Item>
            <Menu.Item
                key={`${uniqueId()}`}
                icon={<DeleteOutlined/>}
                onClick={Delete}
            >
                Delete
            </Menu.Item>
        </Menu>
    );
}

export default function CrudDataTable({config}) {
    return (
        <DataTableList
            config={config}
            DropDownRowMenu={DropDownRowMenu}
            ExportItem={ExportItem}
        />
    );
}