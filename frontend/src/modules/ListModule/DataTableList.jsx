import React, {useCallback, useEffect, useState} from "react";
import {Button, Dropdown, Image, Modal, PageHeader, Table} from "antd";
import {EllipsisOutlined, ExclamationCircleOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import {selectListItems} from "@/redux/crud/selectors";
import uniqueId from "@/utils/uinqueId";


export default function DataTableList({config, DropDownRowMenu, ExportItem}) {
    let {entity, dataTableColumns, dataTableTitle} = config;

    const {result: listResult, isLoading: listIsLoading} = useSelector(selectListItems);

    const {pagination, items} = listResult;

    const dispatch = useDispatch();

    const handelDataTableLoad = useCallback((pagination) => {
        dispatch(crud.list(entity, pagination.current));
    }, []);

    useEffect(() => {
        dispatch(crud.list(entity));
    }, []);

    return (<>
        <PageHeader
            onBack={() => window.history.back()}
            title={dataTableTitle}
            ghost={false}
            extra={[<Button onClick={handelDataTableLoad} key={`${uniqueId()}`}>
                Refresh
            </Button>, <ExportItem key={`${uniqueId()}`} config={config}/>,]}
            style={{
                padding: "20px 0px",
            }}
        ></PageHeader>
        <Table
            columns={dataTableColumns}
            rowKey={(item) => item._id}
            dataSource={items}
            pagination={pagination}
            loading={listIsLoading}
            onChange={handelDataTableLoad}
        />
    </>);
}