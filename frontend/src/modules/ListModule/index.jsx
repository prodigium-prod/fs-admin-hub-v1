import React, { useLayoutEffect } from "react";


import { useDispatch } from "react-redux";
import { crud } from "@/redux/crud/actions";

import CrudDataTable from "./CrudDataTable";
import ListLayout from "@/layout/ListLayout";

export default function ListModule({ config, createForm, updateForm, uploadFormItemProps }) {
    const dispatch = useDispatch();

    useLayoutEffect(() => {
        dispatch(crud.resetState());
    }, []);

    return (
        <ListLayout
            config={config}
        >
            <CrudDataTable config={config} />
        </ListLayout>
    );
}