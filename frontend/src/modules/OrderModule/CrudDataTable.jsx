import React, {useState} from "react";

import {Button, DatePicker, Menu, Modal} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import {selectItemById} from "@/redux/crud/selectors";
import {useCrudContext} from "@/context/crud";
import uniqueId from "@/utils/uinqueId";
import DataTableList from "@/modules/OrderModule/DataTableList";

function checkStatusPayment(row){
    const dispatch = useDispatch();
    const { crudContextAction } = useCrudContext();
    const { panel, collapsedBox, modal, readBox, editBox } = crudContextAction;
    const item = useSelector(selectItemById(row._id));
    console.log("ini row",row)
    return (
        <Button>Generate</Button>
    );
}

function DropDownRowMenu({ row, checkStatusPayment }) {
    const dispatch = useDispatch();
    const { crudContextAction } = useCrudContext();
    const { panel, collapsedBox, modal, readBox, editBox } = crudContextAction;
    const item = useSelector(selectItemById(row._id));
    const Show = () => {
        dispatch(crud.currentItem(item));
        panel.open();
        collapsedBox.open();
        readBox.open();
    };
    function Edit() {
        dispatch(crud.currentAction("update", item));
        editBox.open();
        panel.open();
        collapsedBox.open();
    }
    function Delete() {
        dispatch(crud.currentAction("delete", item));
        modal.open();
    }
    return (
        <Menu style={{ width: 100 }}>
            <Menu.Item key={`${uniqueId()}`} icon={<EyeOutlined />} onClick={Show}>
                Show
            </Menu.Item>
            <Menu.Item key={`${uniqueId()}`} icon={<EditOutlined />} onClick={Edit}>
                Edit
            </Menu.Item>
            <Menu.Item key={`${uniqueId()}`} icon={<DeleteOutlined />} onClick={Delete}>
                Delete
            </Menu.Item>
            {/* Add the "Check Payment Status" button inside the dropdown menu */}
            <Menu.Item key={`${uniqueId()}`} onClick={() => checkStatusPayment(row)}>
                Check Payment Status
            </Menu.Item>
        </Menu>
    );
}

export default function CrudDataTable({config}) {
    return (
        <DataTableList
            config={config}
            // DropDownRowMenu={DropDownRowMenu}
            checkStatusPayment={checkStatusPayment}
        />
    );
}