import React, {useCallback, useEffect, useState} from "react";
import {Button, Menu, message, Modal, PageHeader, Table} from "antd";
import {CloseSquareOutlined, ExclamationCircleOutlined, EyeOutlined} from "@ant-design/icons";
import {selectItemById, selectListItems} from "@/redux/crud/selectors";
import uniqueId from "@/utils/uinqueId";
import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import {useCrudContext} from "@/context/crud";
import axios from "axios";
const {confirm} = Modal;


export default function DataTableList({config}) {

    let {entity, dataTableColumns, dataTableTitle} = config;

    const {result: listResult, isLoading: listIsLoading} = useSelector(selectListItems);

    const {pagination, items} = listResult;

    const dispatch = useDispatch();

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [modalMessage, setModalMessage] = useState("");


    const handelDataTableLoad = useCallback((pagination) => {
        dispatch(crud.list(entity, pagination.current));
    }, []);

    useEffect(() => {
        dispatch(crud.list(entity));
    }, []);


    function DropDownRowMenu({row}) {
        const dispatch = useDispatch();
        const {crudContextAction} = useCrudContext();
        const {panel, collapsedBox, modal, readBox, editBox} = crudContextAction;
        const item = useSelector(selectItemById(row._id));
        const Show = () => {
            dispatch(crud.currentItem(item));
            panel.open();
            collapsedBox.open();
            readBox.open();
        };

        function Edit() {
            dispatch(crud.currentAction("update", item));
            editBox.open();
            panel.open();
            collapsedBox.open();
        }

        function Delete() {
            dispatch(crud.currentAction("delete", item));
            modal.open();
        }


        function checkStatusPayment() {
            axios.get(`https://customer.kilapin.com/notif/status/${item.order_id}`)
                .then((response) => {
                    console.log('Post request successful:', response.data);
                    // Handle the response from the server as needed
                    setIsModalVisible(true);
                    setModalMessage(response.data.message);
                })
                .catch((error) => {
                    console.error('Error in POST request:', error);
                    // Handle the error if the request fails
                });
        }

        function cancelOrder() {
            axios.post('http://back-admin.kilapin.com/api/order-booking/status', {order_id : item.order_id, status: "Cancel"})
            // axios.post('http://localhost:8888/api/order-booking/status', {order_id : item.order_id, status: "Cancel"})
                .then((res) => {
                    console.log('Post request successful:', res.data);
                    // Handle the response from the server as needed
                    message.success('Order has been canceled!');
                    setModalMessage(res.data.message);
                    setIsModalVisible(true);
                })
                .catch((error) => {
                    console.error('Error in POST request:', error);
                    message.error('Failed');
                });
        }

        const showCancelConfirmation = () => {
            confirm({
                title: "Cancel Order",
                icon: <ExclamationCircleOutlined/>,
                content: `Are you sure you want to cancel ${item.order_id} ?`,
                onOk: () => cancelOrder(),
            });
        };

        const isCancelStatus = item.status === "Cancel" || item.status_payment === "Paid"; // Check if the status is

        return (
            <Menu>
                {/*<Menu.Item key={`${uniqueId()}`} icon={<EyeOutlined/>} onClick={Show}>*/}
                {/*    Show*/}
                {/*</Menu.Item>*/}
                {/*<Menu.Item key={`${uniqueId()}`} icon={<EditOutlined/>} onClick={Edit}>*/}
                {/*    Edit*/}
                {/*</Menu.Item>*/}
                {/*<Menu.Item key={`${uniqueId()}`} icon={<DeleteOutlined/>} onClick={Delete}>*/}
                {/*    Delete*/}
                {/*</Menu.Item>*/}
                {/* Add the "Check Payment Status" button inside the dropdown menu */}
                {!isCancelStatus && (
                    <Menu.Item key={`${uniqueId()}`} icon={<EyeOutlined/>} onClick={checkStatusPayment}>
                    <Button>
                        Check Payment
                    </Button>
                </Menu.Item>
                )}
                {!isCancelStatus && (
                    // Render the Cancel button only if status is not "Cancel"
                    <Menu.Item key={`${uniqueId()}`} icon={<CloseSquareOutlined />} onClick={showCancelConfirmation}>
                        <Button danger>Cancel</Button>
                    </Menu.Item>
                )}
            </Menu>
        );
    }

    const columnsWithMenu = [...dataTableColumns];
    const dropDownColumn = {
        title: "Actions",
        key: "actions",
        render: (row) => <DropDownRowMenu row={row}/>,
    };

    columnsWithMenu.push(dropDownColumn);

    return (<>
        {isModalVisible && (
            <Modal
                visible={isModalVisible}
                onCancel={() => setIsModalVisible(false)}
                onOk={() => setIsModalVisible(false)}
            >
                {/* Display the modal message */}
                <p>{modalMessage}</p>
            </Modal>
        )}
        <PageHeader
            onBack={() => window.history.back()}
            title={dataTableTitle}
            ghost={false}
            extra={[<Button onClick={handelDataTableLoad} key={`${uniqueId()}`}>
                Refresh
            </Button>]}
            style={{
                padding: "20px 0px",
            }}
        ></PageHeader>
        <Table
            columns={columnsWithMenu}
            rowKey={(item) => item._id}
            dataSource={items}
            pagination={pagination}
            loading={listIsLoading}
            onChange={handelDataTableLoad}
        />
    </>);
}