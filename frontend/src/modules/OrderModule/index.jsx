import React, {useLayoutEffect, useState} from "react";


import { useDispatch } from "react-redux";
import { crud } from "@/redux/crud/actions";

import CrudDataTable from "./CrudDataTable";
import ListLayout from "@/layout/ListLayout";
import CreateOrder from "@/components/CreateForm/CreateOrder";

export default function OrderModule({ config, createForm }) {
    const dispatch = useDispatch();

    useLayoutEffect(() => {
        dispatch(crud.resetState());
    }, []);

    return (
        <ListLayout
            config={config}
            sidePanelBottomContent={
                <CreateOrder config={config} formElements={createForm}/>
            }
        >
            <CrudDataTable config={config}/>
        </ListLayout>
    );
}