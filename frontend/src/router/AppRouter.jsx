import React, {lazy, Suspense} from "react";
import {Redirect, Route, Switch, useLocation} from "react-router-dom";
import {AnimatePresence} from "framer-motion";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";
import PageLoader from "@/components/PageLoader";

const Dashboard = lazy(() =>
    import(/*webpackChunkName:'DashboardPage'*/ "@/pages/Dashboard")
);
const Admin = lazy(() =>
    import(/*webpackChunkName:'AdminPage'*/ "@/pages/Admin")
);

const Order = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/Order")
);
const News = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/News")
);
const Membership = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/Membership")
);
const Voucher = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/Voucher")
);
const Category = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/Category")
);

const Customer = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/Customer")
);

const TrackingCleaner = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/TrackingCleaner")
);
const AdditionalService = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/AdditionalService")
);

const UserAdditional = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/UserAdditional")
);

const OrderBooking = lazy(() =>
    import(/*webpackChunkName:'CustomerPage'*/ "@/pages/OrderBooking")
);
const SelectCustomer = lazy(() =>
    import(/*webpackChunkName:'SelectCustomerPage'*/ "@/pages/SelectCustomer")
);

const Lead = lazy(() => import(/*webpackChunkName:'LeadPage'*/ "@/pages/Lead"));
const Product = lazy(() =>
    import(/*webpackChunkName:'ProductPage'*/ "@/pages/Product")
);

const Logout = lazy(() =>
    import(/*webpackChunkName:'LogoutPage'*/ "@/pages/Logout")
);
const NotFound = lazy(() =>
    import(/*webpackChunkName:'NotFoundPage'*/ "@/pages/NotFound")
);

export default function AppRouter() {
    const location = useLocation();
    return (
        <Suspense fallback={<PageLoader/>}>
            <AnimatePresence exitBeforeEnter initial={false}>
                <Switch location={location} key={location.pathname}>
                    <PrivateRoute path="/" component={Dashboard} exact/>
                    <PrivateRoute component={Order} path="/order" exact/>
                    <PrivateRoute component={News} path="/news" exact/>
                    <PrivateRoute component={Membership} path="/membership" exact/>
                    <PrivateRoute component={Voucher} path="/voucher" exact/>
                    <PrivateRoute component={Category} path="/category" exact/>
                    <PrivateRoute component={AdditionalService} path="/additional-service" exact/>
                    <PrivateRoute component={TrackingCleaner} path="/tracking-cleaner" exact/>
                    <PrivateRoute component={Customer} path="/customer" exact/>
                    <PrivateRoute component={UserAdditional} path="/users-additional" exact/>
                    <PrivateRoute component={OrderBooking} path="/order-booking" exact/>
                    <PrivateRoute
                        component={SelectCustomer}
                        path="/selectcustomer"
                        exact
                    />
                    <PrivateRoute component={Lead} path="/lead" exact/>
                    <PrivateRoute component={Product} path="/product" exact/>
                    <PrivateRoute component={Admin} path="/admin" exact/>
                    <PrivateRoute component={Logout} path="/logout" exact/>
                    <PublicRoute path="/login" render={() => <Redirect to="/"/>}/>
                    <Route
                        path="*"
                        component={NotFound}
                        render={() => <Redirect to="/notfound"/>}
                    />
                </Switch>
            </AnimatePresence>
        </Suspense>
    );
}