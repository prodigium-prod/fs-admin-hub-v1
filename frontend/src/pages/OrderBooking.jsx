import React, {useState} from "react";

import OrderBookingForm from "@/forms/OrderBookingForm";
import OrderModule from "@/modules/OrderModule";
import { CopyToClipboard } from "react-copy-to-clipboard";

function OrderBooking() {
    const [grossAmount, setGrossAmount] = useState(0);
    const [discountPrice, setDiscountPrice] = useState(0);
    const [initialPrice, setInitialPrice] = useState(0);
    const [selectedAddOns, setSelectedAddons] = useState([]);

    const entity = "order-booking";
    const searchConfig = {
        displayLabels: ["order_id"], searchFields: "order_id", outputValue: "_id",
    };

    const panelTitle = "order-booking Panel";
    const dataTableTitle = "order-booking Lists";
    const entityDisplayLabels = ["order_id"];

    const readColumns = [
        {
            title: "Order ID", dataIndex: "order_id",
        },
        {
            title: "Address", dataIndex: "address",
        },
        {
            title: "Name", dataIndex: "user_id", render: (user_id) => {
                // Assuming you have access to a data structure that contains user_id and its associated name
                // Replace this part with your actual data retrieval method.

                // If the user is found, display the user's name; otherwise, display "Unknown User" or any default value.
                return user_id ? user_id.name : "Unknown User";
            },
        },

    ];
    const dataTableColumns = [
        {
            title: "Order ID", dataIndex: "order_id",
        },
        {
            title: "Time", dataIndex: "time",
        },
        {
            title: "Status", dataIndex: "status",
        },
        {
            title: "Address Order", dataIndex: "address",
        },
        {
            title: "Name", dataIndex: "user_id", render: (user_id) => {
                return user_id ? user_id.name : "Unknown User";
            },
        },
        {
            title: "Phone", dataIndex: "user_id", render: (user_id) => {
                return user_id ? user_id.phone : "Unknown User";
            },
        },
        {
            title: "Payment Url",
            dataIndex: "payment_url",
            render: (payment_url) => {
                return (
                    <CopyToClipboard text={payment_url}>
          <span style={{ cursor: "pointer", textDecoration: "underline" }}>
            {payment_url}
          </span>
                    </CopyToClipboard>
                );
            },
        },
        {
            title: "Status Payment", dataIndex: "status_payment",
        },
    ];

    const ADD_NEW_ENTITY = "Add order-booking";
    const DATATABLE_TITLE = "order-booking list";
    const ENTITY_NAME = "order-booking";
    const CREATE_ENTITY = "Create order-booking";
    const UPDATE_ENTITY = "Update order-booking";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        CREATE_ENTITY,
        ADD_NEW_ENTITY,
        UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
    };

    return (<OrderModule
        createForm={
        <OrderBookingForm
            grossAmount={grossAmount}
            onGrossAmountChange={setGrossAmount}
            discountPrice={discountPrice}
            setDiscountPrice={setDiscountPrice}
            setInitialPrice={setInitialPrice}
            initialPrice={initialPrice}
            setSelectedAddons={setSelectedAddons}
            selectedAddOns={selectedAddOns}
        />}
        updateForm={<OrderBookingForm keyState="update" isUpdateForm={true}/>}
        config={config}
    />);
}

export default OrderBooking;