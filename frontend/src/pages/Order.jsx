import React from "react";

import CrudModule from "@/modules/CrudModule";
import OrderForm from "@/forms/OrderForm";

function Order() {
    const entity = "order";
    const searchConfig = {
        displayLabels: ["address", "status_payment", "order_id"],
        searchFields: "address,status_payment,order_id",
        outputValue: "_id",
    };

    const panelTitle = "Order Assign";
    const dataTableTitle = "Order Assign";
    const entityDisplayLabels = ["address"];

    const readColumns = [{
        title: "Order Id", dataIndex: "order_id",
    }, {
        title: "Address", dataIndex: "address",
    }, {
        title: "Time", dataIndex: "time",
    }, {
        title: "Cleaner", dataIndex: "cleaner_id",
    }, {
        title: "Status Payment", dataIndex: "status_payment",
    }, {
        title: "Voucher", dataIndex: "voucher",
    }, {
        title: "Postal Code", dataIndex: "postal_code",
    }, {
        title: "Total Price", dataIndex: "total_price",
    },];
    const dataTableColumns = [{
        title: "Order Id", dataIndex: "order_id",
    }, {
        title: "Address", dataIndex: "address",
    }, {
        title: "Time", dataIndex: "time",
    }, {
        title: "Cleaner", dataIndex: "cleaner_id",
    }, {
        title: "Status Payment", dataIndex: "status_payment",
    }, {
        title: "Voucher", dataIndex: "voucher",
    }, {
        title: "Postal Code", dataIndex: "postal_code",
    },
        {
        title: "Total Price",
            dataIndex: "total_price",
            render: (total_price) => {
                // Assuming the 'price' is in number format (e.g., 100000)
                return new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                }).format(total_price);
            },
        },
        {
            title: "Initial Price",
            dataIndex: "initial_price",
            render: (initial_price) => {
                // Assuming the 'price' is in number format (e.g., 100000)
                return new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                }).format(initial_price);
            },
        },
        {
            title: "Discount Price",
            dataIndex: "total_discount",
            render: (total_discount) => {
                // Assuming the 'price' is in number format (e.g., 100000)
                return new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                }).format(total_discount);
            },
        },
        {
            title: "Cleaner Balance",
            dataIndex: "cleaner_balance",
            render: (cleaner_balance) => {
                // Assuming the 'price' is in number format (e.g., 100000)
                return new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                }).format(cleaner_balance);
            },
        },
    ];

    const ADD_NEW_ENTITY = "Add new order";
    const DATATABLE_TITLE = "Order Assign";
    const ENTITY_NAME = "order";
    const CREATE_ENTITY = "Create order";
    const UPDATE_ENTITY = "Update Order Assign";
    const config = {
        entity, panelTitle, dataTableTitle, ENTITY_NAME, CREATE_ENTITY, // ADD_NEW_ENTITY,
        UPDATE_ENTITY, DATATABLE_TITLE, readColumns, dataTableColumns, searchConfig, entityDisplayLabels,
    };
    return (
        <CrudModule
            createForm={<OrderForm/>}
            updateForm={<OrderForm keyState="update" isUpdateForm={true}/>}
            config={config}
        />);
}

export default Order;