import React from "react";

import CrudModule from "@/modules/CrudModule";
import MembershipForm from "@/forms/MembershipForm";

function Membership() {
    const entity = "membership";
    const searchConfig = {
        displayLabels: ["name", 'point', 'discount'], searchFields: "name", outputValue: "_id",
    };

    const panelTitle = "Membership Panel";
    const dataTableTitle = "Membership Lists";
    const entityDisplayLabels = ["name"];

    const readColumns = [{
        title: "Name", dataIndex: "name",
    }, {
        title: "Point", dataIndex: "point", render: (point) => `${point} EXP`,
    }, {
        title: "Discount", dataIndex: "discount", render: (discount) => `${discount}%`,
    },];
    const dataTableColumns = [{
        title: "Name", dataIndex: "name",
    }, {
        title: "Point", dataIndex: "point", render: (point) => `${point} EXP`,
    }, {
        title: "Discount", dataIndex: "discount", render: (discount) => `${discount}%`,
    },];

    const ADD_NEW_ENTITY = "Add membership";
    const DATATABLE_TITLE = "Membership list";
    const ENTITY_NAME = "membership";
    const CREATE_ENTITY = "Create membership";
    const UPDATE_ENTITY = "Update membership";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        CREATE_ENTITY,
        ADD_NEW_ENTITY,
        UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
    };

    return (<CrudModule
        createForm={<MembershipForm />}
        updateForm={<MembershipForm keyState="update" isUpdateForm={true} />}
        config={config}
    />);
}

export default Membership;
