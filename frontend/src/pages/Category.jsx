import React from "react";

import CrudModule from "@/modules/CrudModule";
import CategoryForm from "@/forms/CategoryForm";

function Category() {
    const entity = "category";
    const searchConfig = {
        displayLabels: ["name"],
        searchFields: "name",
        outputValue: "_id",
    };

    const panelTitle = "Category Panel";
    const dataTableTitle = "Category Lists";
    const entityDisplayLabels = ["name"];

    const readColumns = [
        {
            title: "Name",
            dataIndex: "name",
        },
    ];
    const dataTableColumns = [
        {
            title: "Name",
            dataIndex: "name",
        },
    ];

    const ADD_NEW_ENTITY = "Add category";
    const DATATABLE_TITLE = "Category List";
    const ENTITY_NAME = "category";
    const CREATE_ENTITY = "Create category";
    const UPDATE_ENTITY = "Update category";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        CREATE_ENTITY,
        ADD_NEW_ENTITY,
        UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
    };
    return (
        <CrudModule
            createForm={<CategoryForm />}
            updateForm={<CategoryForm keyState="update" isUpdateForm={true} />}
            config={config}
        />
    );
}

export default Category;
