import React from "react";
import CrudModule from "@/modules/CrudModule";
import TrackingCleanerForm from "@/forms/TrackingCleanerForm";
import ListModule from "@/modules/ListModule";

function TrackingCleaner() {
    const entity = "tracking-cleaner";
    const searchConfig = {
        displayLabels: ["name"],
        searchFields: "name",
        outputValue: "_id",
    };

    const panelTitle = "Tracking Cleaner Panel";
    const dataTableTitle = "Tracking Cleaner Lists";
    const entityDisplayLabels = ["name"];

    const readColumns = [
        {
            title: "Cleaner Name",
            dataIndex: "name",
        },
    ];
    const dataTableColumns = [
        {
            title: "Cleaner Name",
            dataIndex: "name",
        },
        {
            title: "Status",
            dataIndex: "status",
        },
        {
            title: "Datetime",
            dataIndex: "datetime",
        },
        {
            title: "Location",
            dataIndex: "address",
        },
    ];

    // const ADD_NEW_ENTITY = "Add new customer";
    const DATATABLE_TITLE = "customers List";
    const ENTITY_NAME = "tracking-cleaner";
    // const CREATE_ENTITY = "Create customer";
    // const UPDATE_ENTITY = "Update customer";
    const EXPORT_ENTITY = "Export tracking";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        // CREATE_ENTITY,
        // ADD_NEW_ENTITY,
        // UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
        EXPORT_ENTITY
    };
    return (
        <>
            <ListModule
                createForm={<TrackingCleanerForm />}
                updateForm={<TrackingCleanerForm keyState="update" isUpdateForm={true} />}
                config={config}
            />
        </>
    );
}

export default TrackingCleaner;