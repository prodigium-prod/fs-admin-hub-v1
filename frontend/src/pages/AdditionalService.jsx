import React from "react";

import CrudModule from "@/modules/CrudModule";
import AdditionalServiceForm from "@/forms/AdditionalServiceForm";

function AdditionalService() {
    const entity = "additional-service";
    const searchConfig = {
        displayLabels: ["category", "type"],
        searchFields: "category,type",
        outputValue: "_id",
    };

    const panelTitle = "Addons Panel";
    const dataTableTitle = "Addons Lists";
    const entityDisplayLabels = ["type"];

    const readColumns = [
        {
            title: "Category",
            dataIndex: "category",
        },
        {
            title: "Type",
            dataIndex: "type",
        },
        {
            title: "Type Option",
            dataIndex: "type_option",
        },
        {
            title: "Type Class",
            dataIndex: "type_class",
        },
        {
            title: "Unit",
            dataIndex: "unit",
        },
        {
            title: "Price",
            dataIndex: "price",
        },
        // {
        //     title: "Total",
        //     dataIndex: "total",
        // },
    ];
    const dataTableColumns = [
        {
            title: "Category",
            dataIndex: "category",
        },
        {
            title: "Type",
            dataIndex: "type",
        },
        {
            title: "Type Option",
            dataIndex: "type_option",
        },
        {
            title: "Type Class",
            dataIndex: "type_class",
        },
        {
            title: "Unit",
            dataIndex: "unit",
        },
        {
            title: "Price",
            dataIndex: "price",
            render: (price) => {
                // Assuming the 'price' is in number format (e.g., 100000)
                return new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                }).format(price);
            },
        },
        // {
        //     title: "Total",
        //     dataIndex: "total",
        // },
    ];

    const ADD_NEW_ENTITY = "Add new Addons";
    const DATATABLE_TITLE = "Addons List";
    const ENTITY_NAME = "additional-service";
    const CREATE_ENTITY = "Create Addons";
    const UPDATE_ENTITY = "Update Addons";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        CREATE_ENTITY,
        ADD_NEW_ENTITY,
        UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
    };
    return (
        <CrudModule
            createForm={<AdditionalServiceForm />}
            updateForm={<AdditionalServiceForm keyState="update" isUpdateForm={true} />}
            config={config}
        />
    );
}

export default AdditionalService;