import React from "react";

import CrudModule from "@/modules/CrudModule";
import NewsForm from "@/forms/NewsForm";

function News() {
    const entity = "news";
    const searchConfig = {
        displayLabels: ["title", 'desc', 'status', 'image'], searchFields: "title", outputValue: "_id",
    };

    const panelTitle = "News Panel";
    const dataTableTitle = "News Lists";
    const entityDisplayLabels = ["title"];

    const readColumns = [{
        title: "Title", dataIndex: "title",
    }, {
        title: "Desc", dataIndex: "desc",
    }, {
        title: "Status", dataIndex: "status",
    }, {
        title: "Image", dataIndex: "image",
    }, {
        title: "Lang", dataIndex: "lang",
    },];
    const dataTableColumns = [{
        title: "Title", dataIndex: "title",
    }, {
        title: "Desc", dataIndex: "desc",
    }, {
        title: "Status", dataIndex: "status",
    }, {
        title: "Image", dataIndex: "image",
    }, {
        title: "Lang", dataIndex: "lang",
    },

    ];

    const ADD_NEW_ENTITY = "Add news";
    const DATATABLE_TITLE = "News list";
    const ENTITY_NAME = "news";
    const CREATE_ENTITY = "Create news";
    const UPDATE_ENTITY = "Update news";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        CREATE_ENTITY,
        ADD_NEW_ENTITY,
        UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
    };
    const formProps = {
        uploadFormItemProps: true,
    };

    return (<CrudModule
        createForm={<NewsForm {...formProps}/>}
        updateForm={<NewsForm keyState="update" isUpdateForm={true} {...formProps}/>}
        config={config}
    />);
}

export default News;
