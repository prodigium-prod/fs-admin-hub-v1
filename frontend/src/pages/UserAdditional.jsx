import React from "react";

import CrudModule from "@/modules/CrudModule";
import UserAdditionalForm from "@/forms/UserAdditionalForm";

function UserAdditional() {
    const entity = "users-additional";
    const searchConfig = {
        displayLabels: ["name"], searchFields: "name", outputValue: "_id",
    };

    const panelTitle = "users-additional Panel";
    const dataTableTitle = "users-additional Lists";
    const entityDisplayLabels = ["name"];

    const readColumns = [{
        title: "Name", dataIndex: "name",
    }, {
        title: "Phone", dataIndex: "phone",
    }, {
        title: "Address", dataIndex: "address",
    },];
    const dataTableColumns = [{
        title: "Name", dataIndex: "name",
    }, {
        title: "Phone", dataIndex: "phone",
    }, {
        title: "Address", dataIndex: "address",
    },];

    const ADD_NEW_ENTITY = "Add users-additional";
    const DATATABLE_TITLE = "users-additional list";
    const ENTITY_NAME = "users-additional";
    const CREATE_ENTITY = "Create users-additional";
    const UPDATE_ENTITY = "Update users-additional";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        CREATE_ENTITY,
        ADD_NEW_ENTITY,
        UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
    };

    return (<CrudModule
        createForm={<UserAdditionalForm/>}
        updateForm={<UserAdditionalForm keyState="update" isUpdateForm={true}/>}
        config={config}
    />);
}

export default UserAdditional;