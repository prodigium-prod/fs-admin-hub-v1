import React from "react";

import CrudModule from "@/modules/CrudModule";
import VoucherForm from "@/forms/VoucherForm";

function Voucher() {
    const entity = "voucher";
    const searchConfig = {
        displayLabels: ["code"], searchFields: "code", outputValue: "_id",
    };

    const panelTitle = "Voucher Panel";
    const dataTableTitle = "Voucher Lists";
    const entityDisplayLabels = ["code"];

    const readColumns = [{
        title: "tnc", dataIndex: "tnc",
    }, {
        title: "claimFrom", dataIndex: "claimFrom",
    }, {
        title: "claimUntil", dataIndex: "claimUntil",
    }, {
        title: "canBeClaimed", dataIndex: "canBeClaimed",
    }, {
        title: "maxUserUsed", dataIndex: "maxUserUsed",
    }, {
        title: "discount", dataIndex: "discount",
    }, {
        title: "maxDiscount", dataIndex: "maxDiscount",
    }, {
        title: "maxClaims", dataIndex: "maxClaims",
    }, {
        title: "claimed", dataIndex: "claimed",
    }, {
        title: "event", dataIndex: "event",
    }, {
        title: "type", dataIndex: "type",
    }, {
        title: "zone", dataIndex: "zone",
    }, {
        title: "service", dataIndex: "service",
    }, {
        title: "membership", dataIndex: "membership",
    }, {
        title: "event", dataIndex: "event",
    }, {
        title: "Code", dataIndex: "code",
    }, {
        title: "validFrom", dataIndex: "validFrom",
    }, {
        title: "validUntil", dataIndex: "validUntil",
    },
        {
        title: "voucherUsed", dataIndex: "voucherUsed",
    },
        {
            title: "onlyPrice", dataIndex: "onlyPrice",
        },
        {
        title: "Image", dataIndex: "image",
    },
    ];
    const dataTableColumns = [{
        title: "Code", dataIndex: "code",
    }, {
        title: "validFrom", dataIndex: "validFrom",
    }, {
        title: "validUntil", dataIndex: "validUntil",
    }, {
        title: "voucherUsed", dataIndex: "voucherUsed",
    }, {
        title: "Image", dataIndex: "image",
    },];

    const ADD_NEW_ENTITY = "Add voucher";
    const DATATABLE_TITLE = "Voucher list";
    const ENTITY_NAME = "voucher";
    const CREATE_ENTITY = "Create voucher";
    const UPDATE_ENTITY = "Update voucher";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        CREATE_ENTITY,
        ADD_NEW_ENTITY,
        UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
    };

    const formProps = {
        uploadFormItemProps: true,
    };

    return (<CrudModule
        createForm={<VoucherForm {...formProps}/>}
        updateForm={<VoucherForm keyState="update" isUpdateForm={true} {...formProps}/>}
        config={config}
    />);
}

export default Voucher;