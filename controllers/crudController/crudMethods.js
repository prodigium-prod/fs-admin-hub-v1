const User = require("../../models/User")
const pusher = require("../../setup/pusher");
const AdditionalService = require("../../models/AdditionalServices");
// const CleanerDetail = require("../../models/CleanerDetail")

/**
 *  Retrieves a single document by id.
 *  @param {string} req.params.id
 *  @returns {Document} Single Document
 */

exports.read = async (Model, req, res) => {
    try {
        // Find document by id
        const result = await Model.findOne({_id: req.params.id});
        // If no results found, return document not found
        if (!result) {
            return res.status(404).json({
                success: false, result: null, message: "No document found by this id: " + req.params.id,
            });
        } else {
            // Return success resposne
            return res.status(200).json({
                success: true, result, message: "we found this document by this id: " + req.params.id,
            });
        }
    } catch (err) {
        // Server Error
        return res.status(500).json({
            success: false, result: null, message: "Oops there is an Error",
        });
    }
};
/**
 *  Retrieves a single document by id.
 *  @param {string} req.params.id
 *  @returns {Document} Single Document
 */

exports.countBalanceOrder = async (Model, req, res) => {
    let result;
    try {
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        const currentMonth = currentDate.getMonth() - 1;
        console.log("date",currentDate,currentMonth,currentYear)

        const result = await Model.aggregate([
            {
                $addFields: {
                    yearField: { $year: "$updated_at" },
                    monthField: { $month: "$updated_at" },
                }
            },
            {
                $group: {
                    _id: null,
                    totalAmount: { $sum: "$total_price" },
                    totalDiscount: { $sum: "$total_discount" },
                    totalOrder: { $sum: 1 } // Count the documents in each group
                }
            },
            {
                $project: {
                    _id: 0,
                    totalAmount: 1,
                    totalDiscount: 1,
                    totalOrder: 1
                }
            },
        ]);

        console.log(result)

        // Check if the result array is empty
        if (result.length === 0) {
            return res.status(404).json({
                success: false,
                result: null,
                message: "No data found for the current month."
            });
        }

        // Return success response with the calculated totals
        return res.status(200).json({
            success: true,
            result: result[0], // The result should be an array with a single object
            message: "Total amounts, discounts, and orders for the current month."
        });
    } catch (err) {
        // Server Error
        console.error("Error:", err);
        return res.status(500).json({
            success: false,
            result: null,
            message: "Oops, there is an error: " + err.message
        });
    }
};


/**
 *  Creates a Single document by giving all necessary req.body fields
 *  @param {object} req.body
 *  @returns {string} Message
 */

exports.createUserAdditional = async (Model, req, res) => {
    let result;
    try {
        result = await new Model(req.body).save();
        result.isLoggedIn = false
        result.save();
        return res.status(200).json({
            success: true, result, message: "Successfully Created the document in Model ",
        });
        // Returning successfull response
    } catch (err) {
        // If err is thrown by Mongoose due to required validations
        if (err.name === "ValidationError") {
            return res.status(400).json({
                success: false, result: null, message: "Required fields are not supplied",
            });
        } else {
            // Server Error
            return res.status(500).json({
                success: false, result: null, message: "Oops there is an Error",
            });
        }
    }
};

/**
 *  Creates a Single document by giving all necessary req.body fields
 *  @param {object} req.body
 *  @returns {string} Message
 */

exports.updateStatusOrder = async (Model, req, res) => {
    let result;
    try {
        // console.log(req.body)
        result = await Model.findOne({order_id : req.body.order_id})
        result.status = req.body.status
        result.save();
        if (req.body.status === "Done"){
            const cleaner = await User.findOne({_id : result.cleaner_id}).populate("cleaner_detail")
            cleaner.cleaner_detail.status = "Ready"
            cleaner.cleaner_detail.save();
        }
        // console.log(result)
        return res.status(200).json({
            success: true, result, message: "Success updated",
        });
    } catch (err) {
        // If err is thrown by Mongoose due to required validations
        if (err.name === "ValidationError") {
            return res.status(400).json({
                success: false, result: null, message: "Required fields are not supplied",
            });
        } else {
            // Server Error
            return res.status(500).json({
                success: false, result: null, message: "Oops there is an Error",
            });
        }
    }
};


/**
 *  Creates a Single document by giving all necessary req.body fields
 *  @param {object} req.body
 *  @returns {string} Message
 */

exports.validateOrder = async (Model, req, res) => {
    let findOrderByUser = 0;
    let result
    console.log(req.body.voucher)
    try {
        if (req.body.voucher !== "NO"){
            findOrderByUser = await Model.count({user_id : req.body.customer_id, voucher : req.body.voucher, status_payment: "Paid"})
        }
        if (findOrderByUser <= 0){
            result = []
            return res.status(200).json({
                success: true, result, message: "Successfully validate",
            });
        }else{
            return res.status(203).json({
                success: false, result: null, message: "Voucher telah digunakan",
            });
        }
    } catch (err) {
        // If err is thrown by Mongoose due to required validations
        if (err.name === "ValidationError") {
            return res.status(400).json({
                success: false, result: null, message: "Required fields are not supplied",
            });
        } else {
            // Server Error
            return res.status(500).json({
                success: false, result: null, message: "",
            });
        }
    }
};


/**
 *  Creates a Single document by giving all necessary req.body fields
 *  @param {object} req.body
 *  @returns {string} Message
 */

exports.create = async (Model, req, res) => {
    let result;
    try {
        result = await new Model(req.body).save();
        return res.status(200).json({
            success: true, result, message: "Successfully Created the document in Model ",
        });
        // Returning successfull response
    } catch (err) {
        // If err is thrown by Mongoose due to required validations
        if (err.name === "ValidationError") {
            return res.status(400).json({
                success: false, result: null, message: "Required fields are not supplied",
            });
        } else {
            // Server Error
            return res.status(500).json({
                success: false, result: null, message: "Oops there is an Error",
            });
        }
    }
};

/**
 *  Updates a Single document
 *  @param {object, string} (req.body, req.params.id)
 *  @returns {Document} Returns updated document
 */

exports.update = async (Model, req, res) => {

    try {
        let field
        switch (Model.modelName) {
            case "Order":
                field = {
                    ...req.body, // Spread the existing req.body fields
                    status: "Placement", // Include the additional field and its value
                };
                const beforeUpdate = await Model.findOne({_id: req.params.id})
                console.log('asdasd',beforeUpdate)
                if (beforeUpdate.cleaner_id){
                    const cleanerBefore = await User.findOne({_id: beforeUpdate.cleaner_id}).populate("cleaner_detail")
                    cleanerBefore.cleaner_detail.status = "Ready";
                    cleanerBefore.cleaner_detail.save()
                }
                const cleaner = await User.findOne({_id: req.body.cleaner_id}).populate("cleaner_detail")
                cleaner.cleaner_detail.status = "Ready Booking";
                cleaner.cleaner_detail.save();

                console.log("masuk sini" , cleaner)

                break
            default:
                field = {
                    ...req.body, // Spread the existing req.body fields
                };
                break
        }
        // Find document by id and updates with the required fields
        const result = await Model.findOneAndUpdate({_id: req.params.id}, field, {
            new: true, // return the new result instead of the old one
            runValidators: true,
        }).exec();

        return res.status(200).json({
            success: true, result, message: "we update this document by this id: " + req.params.id,
        });

    } catch (err) {
        // If err is thrown by Mongoose due to required validations
        if (err.name === "ValidationError") {
            return res.status(400).json({
                success: false, result: null, message: "Required fields are not supplied",
            });
        } else {
            // Server Error
            return res.status(500).json({
                success: false, result: null, message: "Oops there is an Error",
            });
        }
    }
};

/**
 *  Delete a Single document
 *  @param {string} req.params.id
 *  @returns {string} Message response
 */

exports.delete = async (Model, req, res) => {
    try {
        // Find the document by id and delete it

        // Find the document by id and delete it
        const result = await Model.findOneAndDelete({_id: req.params.id}).exec();
        // If no results found, return document not found
        if (!result) {
            return res.status(404).json({
                success: false, result: null, message: "No document found by this id: " + req.params.id,
            });
        } else {
            return res.status(200).json({
                success: true, result, message: "Successfully Deleted the document by id: " + req.params.id,
            });
        }
    } catch {
        return res.status(500).json({
            success: false, result: null, message: "Oops there is an Error",
        });
    }
};

/**
 *  Get all documents of a Model
 *  @param {Object} req.params
 *  @returns {Object} Results with pagination
 */

exports.list = async (Model, req, res) => {
    const page = req.query.page || 1;
    const limit = parseInt(req.query.items) || 10;
    const skip = page * limit - limit;
    let resultsPromise;
    let countPromise;

    try {
        //  Query the database for a list of all results
        switch (Model.modelName) {
            case "Order":
                resultsPromise = Model.find({
                    booking_type: "Booking", status_payment: "Paid",
                })
                    .skip(skip)
                    .limit(limit)
                    .populate("user_id")
                    .populate("service_id")
                    .sort({createdAt: -1})

                countPromise = Model.count();
                break;
            case "User":
                switch (req.url) {
                    case "/users/list":
                        resultsPromise = Model.find({
                            role: "Kilapin Customer"
                        }) //later, change postal_code with req.params.postal_code
                            .skip(skip)
                            .limit(limit)

                        countPromise = Model.count();
                        break
                    case "/cleaner/list":
                        const filter = {
                            role: "Kilapin Cleaner", "cleaner_detail.status": "Ready"
                        };
                        // console.log(filter)
                        resultsPromise = await User.find({
                            role: "Kilapin Cleaner", // "cleaner_detail.status" : "Ready"
                        })
                            .populate("cleaner_detail")

                        console.log(resultsPromise)
                        countPromise = Model.count();
                        break
                }
                break;

            case "News":
                resultsPromise = Model.find() //later, change postal_code with req.params.postal_code
                    .skip(skip)
                    .limit(limit)
                    .sort({updatedAt: -1})
                countPromise = Model.count();
                break;

            case "Membership":
                resultsPromise = Model.find()
                    .skip(skip)
                    .limit(limit)
                countPromise = Model.count();
                break;
            case "Category":
                resultsPromise = Model.find()
                    .skip(skip)
                    .limit(limit)
                countPromise = Model.count();
                break;
            case "Voucher":
                resultsPromise = Model.find()
                    .sort({createdAt: -1})
                    .skip(skip)
                    .limit(limit)
                countPromise = Model.count();
                break;

            case "TrackingCleaner":
                resultsPromise = Model.find()
                    .sort({createdAt: -1})
                    .skip(skip)
                    .limit(limit)
                countPromise = Model.count();
                break;
            case "AdditionalServices":
                resultsPromise = Model.find()
                    .sort({createdAt: -1})
                    .skip(skip)
                    .limit(limit)
                countPromise = Model.count();
                break;

            default:
                break;
        }


        // Resolving both promises
        const [result, count] = await Promise.all([resultsPromise, countPromise]);
        // Calculating total pages
        const pages = Math.ceil(count / limit);

        // Getting Pagination Object
        const pagination = {page, pages, count};
        if (count > 0) {
            return res.status(200).json({
                success: true, result, pagination, message: "Successfully found all documents",
            });
        } else {
            return res.status(203).json({
                success: false, result: [], pagination, message: "Collection is Empty",
            });
        }
    } catch {
        return res
            .status(500)
            .json({success: false, result: [], message: "Oops there is an Error"});
    }
};


/**
 *  Get all documents of a Model
 *  @param {Object} req.params
 *  @returns {Object} Results with pagination
 */

exports.listUserAdditional = async (Model, req, res) => {
    const page = req.query.page || 1;
    const limit = parseInt(req.query.items) || 10;
    const skip = page * limit - limit;
    let resultsPromise;
    let countPromise;

    try {

        resultsPromise = Model.find({isLoggedIn: false, role: "Kilapin Customer"})
            .sort({createdAt: -1})
            .skip(skip)
            .limit(limit)
        countPromise = Model.count();

        // Resolving both promises
        const [result, count] = await Promise.all([resultsPromise, countPromise]);
        // Calculating total pages
        const pages = Math.ceil(count / limit);

        // Getting Pagination Object
        const pagination = {page, pages, count};
        if (count > 0) {
            return res.status(200).json({
                success: true, result, pagination, message: "Successfully found all documents",
            });
        } else {
            return res.status(203).json({
                success: false, result: [], pagination, message: "Collection is Empty",
            });
        }
    } catch {
        return res
            .status(500)
            .json({success: false, result: [], message: "Oops there is an Error"});
    }
};


/**
 *  Get all documents of a Model
 *  @param {Object} req.params
 *  @returns {Object} Results with pagination
 */

exports.listServiceOption = async (Model, req, res) => {
    let pagination
    const page = req.query.page || 1;
    const limit = parseInt(req.query.items) || 10;
    const skip = page * limit - limit;
    let resultsPromise;
    let countPromise;

    try {
        resultsPromise = Model.find({option: req.params.option})
            .sort({createdAt: -1})
            .skip(skip)
            .limit(limit)
        countPromise = Model.count();

        // Resolving both promises
        const [result, count] = await Promise.all([resultsPromise, countPromise]);
        // Calculating total pages
        // const pages = Math.ceil(count / limit);
        //
        // // Getting Pagination Object
        // const pagination = {page, pages, count};
        if (count > 0) {
            return res.status(200).json({
                success: true, result, pagination, message: "Successfully found all documents",
            });
        } else {
            return res.status(203).json({
                success: false, result: [], pagination, message: "Collection is Empty",
            });
        }
    } catch {
        return res
            .status(500)
            .json({success: false, result: [], message: "Oops there is an Error"});
    }
};

exports.listAdditionalService = async (Model,req, res) => {
    let pagination
    // const page = req.query.page || 1;
    // const limit = parseInt(req.query.items) || 10;
    // const skip = page * limit - limit;
    let resultsPromise;
    let countPromise;

    try {
        resultsPromise = Model.find({category: "General Cleaning"}).sort({type: 1})

        console.log(resultsPromise)

        countPromise = Model.count({category: "General Cleaning"});

        // Resolving both promises
        const [result, count] = await Promise.all([resultsPromise, countPromise]);
        // Calculating total pages
        // const pages = Math.ceil(count / limit);
        //
        // // Getting Pagination Object
        // const pagination = {page, pages, count};
        console.log(count, result)
        if (count > 0) {
            return res.status(200).json({
                success: true, result, pagination, message: "Successfully found all documents",
            });
        } else {
            return res.status(203).json({
                success: false, result: [], pagination, message: "Collection is Empty",
            });
        }
    } catch {
        return res
            .status(500)
            .json({success: false, result: [], message: "Oops there is an Error"});
    }
};

/**
 *  Get all documents of a Model
 *  @param {Object} req.params
 *  @returns {Object} Results with pagination
 */

exports.listOrderWeb = async (Model, req, res) => {
    let pagination
    const page = req.query.page || 1;
    const limit = parseInt(req.query.items) || 10;
    const skip = page * limit - limit;
    let resultsPromise;
    let countPromise;

    try {
        resultsPromise = Model.find({postal_code: "1"})
            .populate('user_id')
            .sort({createdAt: -1})
            .skip(skip)
            .limit(limit)
        countPromise = Model.count();

        // Resolving both promises
        const [result, count] = await Promise.all([resultsPromise, countPromise]);
        // Calculating total pages
        const pages = Math.ceil(count / limit);

        // Getting Pagination Object
        pagination = {page, pages, count};
        if (count > 0) {
            return res.status(200).json({
                success: true, result, pagination, message: "Successfully found all documents",
            });
        } else {
            return res.status(203).json({
                success: false, result: [], pagination, message: "Collection is Empty",
            });
        }
    } catch {
        return res
            .status(500)
            .json({success: false, result: [], message: "Oops there is an Error"});
    }
};


/**
 *  Searching documents with specific properties
 *  @param {Object} req.query
 *  @returns {Array} List of Documents
 */

exports.search = async (Model, req, res) => {
    if (req.query.q === undefined || req.query.q === "" || req.query.q === " ") {
        return res
            .status(202)
            .json({
                success: false, result: [], message: "No document found by this request",
            })
            .end();
    }
    const fieldsArray = req.query.fields.split(",");

    const fields = {$or: []};

    for (const field of fieldsArray) {
        fields.$or.push({[field]: {$regex: new RegExp(req.query.q, "i")}});
    }

    try {
        let results = await Model.find(fields).sort({name: "asc"}).limit(10);

        if (results.length >= 1) {
            return res.status(200).json({
                success: true, result: results, message: "Successfully found all documents",
            });
        } else {
            return res
                .status(202)
                .json({
                    success: false, result: [], message: "No document found by this request",
                })
                .end();
        }
    } catch {
        return res.status(500).json({
            success: false, result: null, message: "Oops there is an Error",
        });
    }
};
//
// const Excel = require("exceljs");
//
// exports.export = async (Model, req, res) => {
//     try {
//
//         const { filter } = req.body;
//         const { fields } = filter;
//
//         let fieldsArray = ['name','cleaner_id','status','lat_long','address','datetime'];
//
//         let results = await Model.find();
//         // console.log(results);
//
//         if (results.length >= 1) {
//             // Create Excel workbook and worksheet
//             const workbook = new Excel.Workbook();
//             const worksheet = workbook.addWorksheet("Data");
//             console.log(fieldsArray)
//             // Add header row
//             const headerRow = worksheet.addRow(fieldsArray);
//             console.log(headerRow)
//             headerRow.font = { bold: true };
//
//             // Add data rows
//             results.forEach((result) => {
//                 const rowData = fieldsArray.map((field) => result[field] || "");
//                 console.log(rowData)
//                 worksheet.addRow(rowData);
//             });
//
//             // Set response headers for Excel download
//             // res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
//             // res.setHeader(
//             //     "Content-Disposition",
//             //     `attachment; filename=${Model.modelName}_export.xlsx`
//             // );
//             // Stream Excel data to the response
//             workbook.xlsx.writeBuffer().then((dataBuffer) => {
//                 // Set response headers for Excel download
//                 res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
//                 res.setHeader(
//                     "Content-Disposition",
//                     `attachment; filename=${Model.modelName}_export.xlsx`
//                 );
//                 // Stream Excel data to the response
//                 res.end(dataBuffer);
//             });
//         } else {
//             return res.status(202).json({
//                 success: false,
//                 result: [],
//                 message: "No document found by this request",
//             });
//         }
//     } catch (error) {
//         console.error("Export error:", error);
//         return res.status(500).json({
//             success: false,
//             result: null,
//             message: "Oops, there is an error",
//         });
//     }
// };