const crudMethods = require("./crudMethods");
const mongoose = require("mongoose");

exports.crudController = (modelName) => {
  const Model = mongoose.model(modelName);
  let methods = {};

  methods.create = async (req, res) => {
    crudMethods.create(Model, req, res);
  };

  methods.updateStatusOrder = async (req, res) => {
    crudMethods.updateStatusOrder(Model, req, res);
  };

  methods.validateOrder = async (req, res) => {
    crudMethods.validateOrder(Model, req, res);
  };

  methods.createUserAdditional = async (req, res) => {
    crudMethods.createUserAdditional(Model, req, res);
  };

  methods.read = async (req, res) => {
    crudMethods.read(Model, req, res);
  };
  methods.countBalanceOrder = async (req, res) => {
    crudMethods.countBalanceOrder(Model, req, res);
  };

  methods.update = async (req, res) => {
    crudMethods.update(Model, req, res);
  };

  methods.delete = async (req, res) => {
    crudMethods.delete(Model, req, res);
  };

  methods.list = async (req, res) => {
    const model = modelName;
    crudMethods.list(Model, req, res);
  };

  methods.listUserAdditional = async (req, res) => {
    // console.log("masuk sini",modelName)
    await crudMethods.listUserAdditional(Model, req, res);
  };

  methods.listAdditionalService = async (req, res) => {
    // console.log("masuk sini",modelName)
    await crudMethods.listAdditionalService(Model, req, res);
  };

  methods.listServiceOption = async (req, res) => {
    // console.log("masuk sini",modelName)
    await crudMethods.listServiceOption(Model, req, res);
  };
  methods.listOrderWeb = async (req, res) => {
    // console.log("masuk sini",modelName)
    await crudMethods.listOrderWeb(Model, req, res);
  };

  methods.search = async (req, res) => {
    crudMethods.search(Model, req, res);
  };

  methods.export = async (req, res) => {
    crudMethods.export(Model, req, res);
  };

  return methods;
};